function gui_fmt=init_new_gui_fmt_struct()


gui_fmt.x_sep=3;
gui_fmt.y_sep=3;
gui_fmt.txt_w=125;
gui_fmt.txt_h=18;
gui_fmt.box_w=30;
gui_fmt.box_h=20;
gui_fmt.button_w=60;
gui_fmt.button_h=20;


gui_fmt.txtStyle=struct('HorizontalAlignment','right','BackgroundColor','white','fontsize',11);
gui_fmt.txtTitleStyle=struct('HorizontalAlignment','center','BackgroundColor','white','Fontweight','Bold','fontsize',11);
gui_fmt.edtStyle=struct('BackgroundColor','white','fontsize',11);
gui_fmt.pushbtnStyle=struct('fontsize',11);
gui_fmt.chckboxStyle=struct('BackgroundColor','white','fontsize',11);
gui_fmt.popumenuStyle=struct('fontsize',11);
gui_fmt.lstboxStyle=struct('fontsize',11);
gui_fmt.radbtnStyle=struct('BackgroundColor','white','fontsize',11);
