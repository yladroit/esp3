function usrdata=init_echo_usrdata()

usrdata.CID='';
usrdata.Fieldname='';
usrdata.Layer_ID='';
usrdata.Idx_r=[];
usrdata.Idx_ping=[];
usrdata.ax_tag='main';
usrdata.geometry_y='samples';
usrdata.geometry_x='pings';
usrdata.xlim=[nan nan];
usrdata.ylim=[nan nan];
end