function display_in_3D_callback(~,~,main_figure,IDs,tt)

% get current layers
layers = get_esp3_prop('layers');
if isempty(layers)
    return;
end

% find layers to export
if ~iscell(IDs)f
    IDs = {IDs};
end

if isempty(IDs{1})
    % empty IDs means do all layers
    layers_to_disp = layers;
else
    % else, find the layers with input IDs
    idx = [];
    for id = 1:length(IDs)
        [idx_temp,found] = find_layer_idx(layers,IDs{id});

        if found == 0
            continue;
        end
        idx = union(idx,idx_temp);
    end
    layers_to_disp = layers(idx);
end

curr_disp = get_esp3_prop('curr_disp');

echo_3D_obj = get_esp3_prop('echo_3D_obj');

if isempty(echo_3D_obj)
    echo_3D_obj = echo_3D_cl('cmap',curr_disp.Cmap);
    esp3_obj=getappdata(groot,'esp3_obj');
    esp3_obj.echo_3D_obj = echo_3D_obj;
end

switch tt
    case'curtain'
        for ilay = 1:numel(layers_to_disp)
            [trans_obj,idxt]=layers_to_disp(ilay).get_trans(curr_disp);

            if isempty(idxt)
                trans_obj = layers_to_disp(ilay).Transceivers(1);
            end

            if ~isempty(layers_to_disp(ilay).SurveyData)
                for is=1:length(layers_to_disp(ilay).SurveyData)
                    survd=layers_to_disp(ilay).get_survey_data('Idx',is);
                    echo_3D_obj.add_surface(trans_obj,'surv_data',survd,'cax',curr_disp.getCaxField('sv'));
                end
            else
                echo_3D_obj.add_surface(trans_obj);
            end

        end
    case 'bathy'
        echo_3D_obj.add_bathy(layers_to_disp,'tag','bathy');

    case {'feature_ID_scatter' 'feature_sv_scatter'}  
        if strcmpi(tt,'feature_ID_scatter')
            ff = 'feature_id';
        else
            ff = 'feature_sv';
        end
        for ilay = 1:numel(layers_to_disp)
            [trans_obj,idxt]=layers_to_disp(ilay).get_trans(curr_disp);

            if isempty(idxt)
                trans_obj = layers_to_disp(ilay).Transceivers(1);
            end

            cax=curr_disp.getCaxField(ff);
            echo_3D_obj.add_feature(trans_obj,'fieldname',ff,'cax',cax);
        end
        
end
