
function update_wc_fig(layer,ip)

esp3_obj = getappdata(groot,'esp3_obj');
wc_fan  = getappdata(esp3_obj.main_figure,'wc_fan');

if isempty(wc_fan)
    return;
end

curr_disp = esp3_obj.curr_disp;
[trans_obj,~]=layer.get_trans(curr_disp);

if trans_obj.ismb
    fname = list_layers(layer,'valid_filename',false);
    [~,fnamet,~] = fileparts(fname{1});
    tt = sprintf('File: %s. Ping: %.0f/%.0f. Time: %s.',fnamet,ip,numel(trans_obj.Time),datestr(trans_obj.get_transceiver_time(ip),'HH:MM:SS'));
    disp_ping_wc_fan(wc_fan,trans_obj,'ip',ip,'curr_disp',esp3_obj.curr_disp,'tt',tt);
else
    clean_echo_figures(esp3_obj.main_figure,'Tag','wc_fan');
    rmappdata(esp3_obj.main_figure,'wc_fan');
end

end

