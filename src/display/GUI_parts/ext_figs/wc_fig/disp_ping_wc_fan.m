function disp_ping_wc_fan(wc_fan,trans_obj,varargin)

p = inputParser;
addRequired(p,'wc_fan',@isstruct);
addRequired(p,'trans_obj',@(x) isa(x,'transceiver_cl'));
addParameter(p,'mask',[]);
addParameter(p,'curr_disp',[]);
addParameter(p,'tt','');
addParameter(p,'ip',1,@(x) x>0);
addParameter(p,'idx_beam',[]);
addParameter(p,'idx_r',[]);

parse(p,wc_fan,trans_obj,varargin{:});

mask =  p.Results.mask;
curr_disp = p.Results.curr_disp;
ip = p.Results.ip;
tt = p.Results.tt;
if isempty(curr_disp)
    curr_disp  = curr_state_disp_cl();
end

if ~trans_obj.ismb
    return;
end

cax = curr_disp.Cax;
if ~isempty(ip)

    [amp,sampleAcrossDist,sampleUpDist]=trans_obj.get_subdatamat_AcUp_pos('field',curr_disp.Fieldname,'idx_ping',ip,'idx_beam',p.Results.idx_beam,'idx_r',p.Results.idx_r);

    if isempty(amp)
        return;
    end
    amp  =squeeze(amp);
    if numel(size(mask)) == 3
        mask = squeeze(mask);
    end
    idx_keep_cax = amp>cax(1);
    
    if isempty(mask)
        idx_keep = amp>cax(1);
    else
        idx_keep = amp>cax(1) & mask;
    end


    r_bot = squeeze(trans_obj.get_bottom_range(ip));
    beamAngle=trans_obj.get_params_value('BeamAngleAthwartship',ip,p.Results.idx_beam);

    ac_bot = -r_bot.*sind(squeeze(beamAngle));
    up_bot = r_bot.*cosd(squeeze(beamAngle));


    set(wc_fan.bot_gh,...
        'XData',ac_bot,...
        'YData',up_bot);

    % display WC data itself
    set(wc_fan.wc_gh,...
        'XData',squeeze(sampleAcrossDist),...
        'YData',squeeze(sampleUpDist),...
        'ZData',zeros(size(amp),'int8'),...
        'CData',amp,...
        'AlphaData',idx_keep);
    
    beamAngle=trans_obj.get_params_value('BeamAngleAthwartship',ip);
    if ~any(idx_keep_cax)
        idx_keep_cax  = true(size(amp));
    end

    ylim = [0 max(max(sampleUpDist(idx_keep_cax),[],"all",'omitnan'),max(up_bot))];
    ylim(1) = max(ylim(1),curr_disp.R_disp(1)*cosd(max(abs(beamAngle),[],'omitnan')),'omitnan');
    ylim(2) = min(ylim(2),curr_disp.R_disp(2),'omitnan');

    idx_keep_up = sampleUpDist>=ylim(1) &sampleUpDist<=ylim(2);
    xlim = [min(sampleAcrossDist(idx_keep_up)) max(sampleAcrossDist(idx_keep_up))];

    if ylim(2)>10
        wc_fan.wc_axes.XAxis.TickLabelFormat='%.0fm';
        wc_fan.wc_axes.YAxis.TickLabelFormat='%.0fm';
    elseif ylim(2)<=1
        wc_fan.wc_axes.XAxis.TickLabelFormat='%.2fm';
        wc_fan.wc_axes.YAxis.TickLabelFormat='%.2fm';
    else
        wc_fan.wc_axes.XAxis.TickLabelFormat='%.1fm';
        wc_fan.wc_axes.YAxis.TickLabelFormat='%.1fm';
    end


    if diff(ylim)>0
        set(wc_fan.wc_axes,...
            'YLim',ylim);
    end

    if diff(xlim)>0
        set(wc_fan.wc_axes,...
            'XLim',xlim);

    end
    set(wc_fan.wc_axes,...
        'CLim',curr_disp.Cax,...
        'Layer','top','Visible','on');
    if isempty(tt)
        tt = sprintf('Ping: %.0f/%.0f. Time: %s.',ip,numel(trans_obj.Time),datestr(trans_obj.get_transceiver_time(ip),'HH:MM:SS'));
    end
    wc_fan.wc_axes_tt.String = tt;
else
    amp = wc_fan.wc_gh.CData;

    if isempty(mask)
        idx_keep = amp>cax(1);
    else
        idx_keep = amp>cax(1) & mask;
    end
    % display WC data itself

    set(wc_fan.wc_gh,...
        'AlphaData',idx_keep);

    wc_fan.wc_axes.CLim = curr_disp.Cax;
end