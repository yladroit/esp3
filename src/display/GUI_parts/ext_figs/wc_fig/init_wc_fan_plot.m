function init_wc_fan_plot()

esp3_obj = getappdata(groot,'esp3_obj');


lay = get_current_layer();
if ~isempty(lay)
    trans_obj = lay.get_trans(esp3_obj.curr_disp);
else
    return;
end

if ~trans_obj.ismb
    clean_echo_figures(esp3_obj.main_figure,'Tag','wc_fan');
    if isappdata(esp3_obj.main_figure,'wc_fan')
        rmappdata(esp3_obj.main_figure,'wc_fan');
    end
    return;
end

if ~isappdata(esp3_obj.main_figure,'wc_fan')
    wc_fan_fig = new_echo_figure(esp3_obj.main_figure,...
        'Name','WC fan',...
        'tag','wc_fan',...
        'UiFigureBool',true,...
        'CloseRequestFcn',@rm_wc_fan_appdata);
    
    wc_fan = create_wc_fan('wc_fig',wc_fan_fig,'curr_disp',esp3_obj.curr_disp);
    
    setappdata(esp3_obj.main_figure,'wc_fan',wc_fan);
else
    wc_fan = getappdata(esp3_obj.main_figure,'wc_fan');
end

wc_fan.wc_fan_fig.Visible = 'on';

update_wc_fig(lay,1);


end


function rm_wc_fan_appdata(src,evt)

esp3_obj = getappdata(groot,'esp3_obj');
rmappdata(esp3_obj.main_figure,'wc_fan');

delete(src)

end