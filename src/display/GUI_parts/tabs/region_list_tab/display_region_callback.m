function display_region_callback(src,~,main_figure,opt)
layer=get_current_layer();

if isempty(layer)
    return;
end

curr_disp=get_esp3_prop('curr_disp');

[trans_obj,~]=layer.get_trans(curr_disp);
load_bar_comp=getappdata(main_figure,'Loading_bar');

for i=1:length(curr_disp.Active_reg_ID)
    
    reg_curr=trans_obj.get_region_from_Unique_ID(curr_disp.Active_reg_ID{i});
    
    if isempty(reg_curr)
        return;
    end
    line_obj=[];
    field = 'sv';
    switch opt
        case '2D'
            switch reg_curr.Reference
                case 'Line'
                    line_obj=layer.get_first_line();
            end
            
            if ismember('svdenoised',trans_obj.Data.Fieldname)
                field='svdenoised';
            else
                field='sv';
            end
            
            show_status_bar(main_figure);
            
            reg_curr.display_region(trans_obj,'main_figure',main_figure,'line_obj',line_obj,'field',field,'load_bar_comp',load_bar_comp);
        otherwise
            switch opt
                
                case '3D'
                    if ismember('spdenoised',trans_obj.Data.Fieldname)
                        field='spdenoised';
                    else
                        field='sp';
                    end
                case '3D_sv'
                    if ismember('svdenoised',trans_obj.Data.Fieldname)
                        field='svdenoised';
                    else
                        field='sv';
                    end
                case '3D_curr_field'
                    field = curr_disp.Fieldname;
                case '3D_ST'
                    field='singletarget';
                case '3D_tracks'
                    field = 'trackedtarget';
            end
            reg_curr.display_region_3D(trans_obj,[],'main_figure',main_figure,'field',field,'load_bar_comp',load_bar_comp);
    end
       
    hide_status_bar(main_figure);
    
end
end