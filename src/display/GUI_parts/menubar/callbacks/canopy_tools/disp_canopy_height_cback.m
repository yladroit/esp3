function disp_canopy_height_cback(src,evt)

layers = get_esp3_prop('layers');
main_figure = get_esp3_prop('main_figure');

if isempty(layers)
    return;
end
r_cal = 100;
map_fig = new_echo_figure(main_figure,'UiFigureBool',true,...
    'Name','Canopy Height Map','tag','canopyHeightmap');
gl  = uigridlayout(map_fig,[1 3]);

map_fig_density = new_echo_figure(main_figure,'UiFigureBool',true,...
    'Name','Plant Density Estimation','tag','canopyHeightmap_density');
gl_density  = uigridlayout(map_fig_density,[2 2]);
gl_density.RowHeight = {'1x',50};


curr_disp=get_esp3_prop('curr_disp');
base_curr=curr_disp.Basemap;

gax = geoaxes('Parent', gl,...
    'Tag','Heightmap','basemap', base_curr);
gax.Layout.Row = 1;
gax.Layout.Column = 1;
format_geoaxes(gax);
title(gax,'Canopy Height(m)');
[cmap,col_ax,col_lab,col_grid,col_bot,col_txt,~]=init_cmap(curr_disp.Cmap);
colormap(gax,cmap);

gax_bio = geoaxes('Parent', gl,...
    'Tag','biovolume','basemap', base_curr);
gax_bio.Layout.Row = 1;
gax_bio.Layout.Column = 2;
format_geoaxes(gax_bio);
title(gax_bio,'Biovolume(%)');
[cmap,col_ax,col_lab,col_grid,col_bot,col_txt,~]=init_cmap('viridis');
colormap(gax_bio,cmap);

gax_bathy = geoaxes('Parent', gl,...
    'Tag','Bathymap','basemap', base_curr);
gax_bathy.Layout.Row = 1;
gax_bathy.Layout.Column = 3;
format_geoaxes(gax_bathy);
title(gax_bathy,'Bathymetry(m)');
[cmap,col_ax,col_lab,col_grid,col_bot,col_txt,~]=init_cmap('GMT_drywet');
colormap(gax_bathy,cmap);

gax_density = geoaxes('Parent', gl_density,...
    'Tag','Heightmap_density','basemap', base_curr);
gax_density.Layout.Row = 1;
gax_density.Layout.Column = 1;
format_geoaxes(gax_density);
title(gax_density,'Canopy Height relative density(m)');
[cmap,col_ax,col_lab,col_grid,col_bot,col_txt,~]=init_cmap(curr_disp.Cmap);
colormap(gax_density,cmap);

gax_bio_density = geoaxes('Parent', gl_density,...
    'Tag','biovolume_density','basemap', base_curr);
gax_bio_density.Layout.Row = 1;
gax_bio_density.Layout.Column = 2;
format_geoaxes(gax_bio_density);
title(gax_bio_density,'Biovolume relative density(%)');
[cmap,col_ax,col_lab,col_grid,col_bot,col_txt,~]=init_cmap('viridis');
colormap(gax_bio_density,cmap);

gl_ctrl  = uigridlayout(gl_density,[1 3]);
gl_ctrl.Layout.Row = 2;
gl_ctrl.Layout.Column = [1 2];
gl_ctrl.ColumnWidth = {'2x',50,'1x'};

text_radius = uilabel(gl_ctrl,'Text','Radius used for density calculation (m): ');
text_radius.Layout.Row = 1;
text_radius.Layout.Column = 1;
text_radius.HorizontalAlignment = 'right';

edit_radius = uieditfield(gl_ctrl, 'numeric');
edit_radius.Limits = [1 500];
edit_radius.ValueDisplayFormat = '%.0f';
edit_radius.Layout.Row = 1;
edit_radius.Layout.Column = 2;
edit_radius.Value = r_cal;

[basemap_list,~,~,basemap_dispname_list]=list_basemaps(0,curr_disp.Online,curr_disp.Basemaps);

basemap_list_h = uidropdown(gl_ctrl,'Items',basemap_dispname_list,'ItemsData',basemap_list,...
                     'Value',base_curr);
basemap_list_h.Layout.Row = 1;
basemap_list_h.Layout.Column = 3;

cbar = colorbar(gax,'southoutside');
cbar_bio = colorbar(gax_bio,'southoutside');
cbar_bathy = colorbar(gax_bathy,'southoutside');

cbar_bio_density = colorbar(gax_bio_density,'southoutside');
cbar_density = colorbar(gax_density,'southoutside');

data_struct.Bathy = [];
data_struct.Height = [];
data_struct.Lat = [];
data_struct.Lon = [];

win_size = 21;

for uilay = 1 : numel(layers)
    trans_obj = layers(uilay).get_trans(curr_disp);
    idx = layers(uilay).get_lines_per_Tag('canopy');

    if isempty(trans_obj) || isempty(layers(uilay).Lines) || isempty(idx)
        continue;
    end

    line_obj = layers(uilay).Lines(idx(1));
    curr_dist=trans_obj.GPSDataPing.Dist(:)';

    [~,~,r_line] = line_obj.get_time_dist_and_range_corr(trans_obj.get_transceiver_time(),curr_dist);

    r_bot = trans_obj.get_bottom_range();
    d_bot = trans_obj.get_bottom_depth();

    if numel(r_bot)>2*win_size
        d_bot = filter2_perso(gausswin(win_size)',d_bot);
        r_bot = filter2_perso(gausswin(win_size)',r_bot);
    end

    data_struct.Bathy = [data_struct.Bathy d_bot];
    data_struct.Height = [data_struct.Height r_bot-r_line];
    data_struct.Lat = [data_struct.Lat trans_obj.GPSDataPing.Lat];
    data_struct.Lon = [data_struct.Lon trans_obj.GPSDataPing.Long];

end
data_struct.Height(data_struct.Height<0) = 0;

if isempty(data_struct.Lon)
    return;
end

data_struct.Biovolume_prc = data_struct.Height./data_struct.Bathy*100;

s_obj=geoscatter(gax,data_struct.Lat,data_struct.Lon,6,data_struct.Height,'filled');
s_obj_bio=geoscatter(gax_bio,data_struct.Lat,data_struct.Lon,6,data_struct.Biovolume_prc,'filled');
s_obj_bathy=geoscatter(gax_bathy,data_struct.Lat,data_struct.Lon,6,data_struct.Bathy,'filled');

s_obj_density=geodensityplot(gax_density,data_struct.Lat,data_struct.Lon,data_struct.Height,'FaceColor','interp','Radius',r_cal);
s_obj_bio_density=geodensityplot(gax_bio_density,data_struct.Lat,data_struct.Lon,data_struct.Biovolume_prc,'FaceColor','interp','Radius',r_cal);
edit_radius.ValueChangedFcn = {@change_edit_radius,[s_obj_bio_density s_obj_density]};
basemap_list_h.ValueChangedFcn = {@change_basemap,[gax gax_bathy gax_bio gax_bio_density gax_density]};
bv_min = 5;
h_min = 0.25;

set(s_obj,'MarkerFaceAlpha','flat','AlphaDataMapping','scaled');
s_obj.AlphaData = double(data_struct.Height>h_min);
set(s_obj_bathy,'MarkerFaceAlpha',0.5,'MarkerEdgeAlpha',0.5);
set(s_obj_bio,'MarkerFaceAlpha','flat','AlphaDataMapping','scaled');
s_obj_bio.AlphaData = double(data_struct.Biovolume_prc>bv_min);
LatLim = [min(data_struct.Lat,[],"all","omitnan") max(data_struct.Lat,[],"all","omitnan")];
LonLim = [min(data_struct.Lon,[],"all","omitnan") max(data_struct.Lon,[],"all","omitnan")];
[LatLim,LongLim] = ext_lat_lon_lim_v2(LatLim,LonLim,0.2);

geolimits(gax,LatLim,LongLim);
geolimits(gax_bathy,LatLim,LongLim);
geolimits(gax_bio,LatLim,LongLim);
geolimits(gax_density,LatLim,LongLim);
geolimits(gax_bio_density,LatLim,LongLim);

caxis(gax,prctile(data_struct.Height,[50 95]));
caxis(gax_bio,prctile(data_struct.Biovolume_prc(data_struct.Biovolume_prc>bv_min),[10 80]));
caxis(gax_bathy,prctile(data_struct.Bathy,[2 95]));

caxis(gax_density,prctile(data_struct.Height,[50 95]));
caxis(gax_bio_density,prctile(data_struct.Biovolume_prc(data_struct.Biovolume_prc>bv_min),[10 80]));

drawnow();

cbar.TickLabels = cellfun(@(x) sprintf('%s m',x),cbar.TickLabels,'UniformOutput',0);
cbar_bathy.TickLabels = cellfun(@(x) sprintf('%s m',x),cbar_bathy.TickLabels,'UniformOutput',0);
cbar_bio.TickLabels = cellfun(@(x) sprintf('%s %s',x,'%'),cbar_bio.TickLabels,'UniformOutput',0);
cbar_density.TickLabels = cellfun(@(x) sprintf('%s m',x),cbar_density.TickLabels,'UniformOutput',0);
cbar_bio_density.TickLabels = cellfun(@(x) sprintf('%s %s',x,'%'),cbar_bio_density.TickLabels,'UniformOutput',0);


war_str=sprintf('Do you want to save the results?');
choice=question_dialog_fig(main_figure,'',war_str,'timeout',10);
switch lower(choice)
    case 'yes'
        % output file name
        [path_lay,~] = layers.get_path_files();
        output_fullfile = fullfile(path_lay{1},'canopy_data.shp');
        [filename, pathname] = uiputfile('*.shp','Export Canopy data to shapefile and .csv',output_fullfile);
        output_fullfile = fullfile(pathname,filename);
        output_fullfile_csv = strrep(output_fullfile,'.shp','.csv');
        output = data_struct;
        ff=fieldnames(output);

        for idi=1:numel(ff)
            if isrow(output.(ff{idi}))
                output.(ff{idi})=output.(ff{idi})';
            end
        end
        struct2csv(output,output_fullfile_csv);

        data_struct_shp = cell(1,numel(data_struct.Bathy));
        for il = 1:numel(data_struct.Bathy)
            for idi=1:numel(ff)
                data_struct_shp{il}.(ff{idi})=data_struct.(ff{idi})(il);
            end
            data_struct_shp{il}.Geometry = 'Point';
        end

        shapewrite(vertcat(data_struct_shp{:}),output_fullfile);
        warndlg_perso(main_figure,'Done','Canopy Height estimation finished and exported...');
end
end

function change_edit_radius(src,evt,hh)
for ui = 1:numel(hh)
    hh(ui).Radius = src.Value;
end
end

function change_basemap(src,evt,hh)
for ui = 1:numel(hh)
    hh(ui).Basemap = src.Value;
end
end

