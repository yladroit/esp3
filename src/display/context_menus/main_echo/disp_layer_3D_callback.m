function disp_layer_3D_callback(~,~,main_figure,rem)

layer=get_current_layer();
axes_panel_comp=getappdata(main_figure,'Axes_panel');
curr_disp=get_esp3_prop('curr_disp');
[trans_obj,~]=layer.get_trans(curr_disp);
trans=trans_obj;

ax_main=axes_panel_comp.echo_obj.main_ax;
x_lim=double(get(ax_main,'xlim'));

cp = ax_main.CurrentPoint;
x=cp(1,1);

x=nanmax(x,x_lim(1));
x=nanmin(x,x_lim(2));

xdata=trans.get_transceiver_pings();

[~,idx_ping]=nanmin(abs(xdata-x));

t_n=trans.Time(idx_ping);

[surv_to_modif,~]=layer.get_survdata_at_time(t_n);

[trans_obj,found]=layer.get_trans(curr_disp);

if ~found
    return;
end
echo_3D_obj = get_esp3_prop('echo_3D_obj');

if isempty(echo_3D_obj)&&rem
    return;
end

if isempty(echo_3D_obj)
    echo_3D_obj = echo_3D_cl('cmap',curr_disp.Cmap);
    esp3_obj=getappdata(groot,'esp3_obj');
    esp3_obj.echo_3D_obj = echo_3D_obj;
end

if ~rem
    echo_3D_obj.add_surface(trans_obj,'surv_data',surv_to_modif,'cax',curr_disp.getCaxField('sv'));
else
    echo_3D_obj.rem_surface(trans_obj,'surv_data',surv_to_modif)
end


