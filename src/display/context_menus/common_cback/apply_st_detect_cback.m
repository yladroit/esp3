%% apply_st_detect_cback.m
%
% Apply single target detection on selected area or region_cl
%
%% Help
%
% *USE*
%
% TODO
%
% *INPUT VARIABLES*
%
% * |select_plot|: TODO
% * |main_figure|: Handle to main ESP3 window
%
% *OUTPUT VARIABLES*
%
% NA
%
% *RESEARCH NOTES*
%
% TODO
%
% *NEW FEATURES*
%
% * 2017-03-22: header and comments updated according to new format (Alex Schimel)
% * 2017-03-02: first version (Yoann Ladroit)
%
% *EXAMPLE*
%
% TODO
%
% *AUTHOR, AFFILIATION & COPYRIGHT*
%
% Yoann Ladroit, NIWA. Type |help EchoAnalysis.m| for copyright information.

%% Function
function apply_st_detect_cback(~,~,select_plot,main_figure)

update_algos('algo_name',{'SingleTarget'});
layer=get_current_layer();
curr_disp=get_esp3_prop('curr_disp');
alg_name='SingleTarget';


[trans_obj,~]=layer.get_trans(curr_disp);

switch class(select_plot)
    case 'region_cl'
        select_plot=trans_obj.get_region_from_Unique_ID(curr_disp.Active_reg_ID);
end

if isempty(select_plot)
    return;
end

show_status_bar(main_figure);
load_bar_comp=getappdata(main_figure,'Loading_bar');


trans_obj.apply_algo(alg_name,'load_bar_comp',load_bar_comp,'reg_obj',select_plot);


hide_status_bar(main_figure);
curr_disp.setField('singletarget');
display_tracks(main_figure);
update_st_tracks_tab(main_figure,'histo',1,'st',1);



end