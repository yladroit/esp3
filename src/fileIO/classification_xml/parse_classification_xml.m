function [Frequencies,Variables,Nodes,Title,Type,VarName]=parse_classification_xml(xml_file)

xml_struct=parseXML(xml_file);
Title='';
Type='';
Frequencies=[];
Variables={};
Nodes={};
VarName = '';
if ~strcmpi(xml_struct.Name,'classification_descr')
    warning('XML file not describing a Classification');
    return;
end

Title=get_att(xml_struct,'title');
if isempty(Title)
    Title='';
end

Type=get_att(xml_struct,'type');
if isempty(Type)
    Type='By regions';
end

VarName=get_att(xml_struct,'varname');


nb_child=length(xml_struct.Children);

for ic=1:nb_child
    switch xml_struct.Children(ic).Name
        case 'variables'
           Variables=get_variables(xml_struct.Children(ic));
        case 'nodes'
           Nodes=get_nodes(xml_struct.Children(ic));
        case 'frequencies'
          Frequencies=str2double(strsplit(xml_struct.Children(ic).Data,';'));
        otherwise
            warning('Unidentified Child in XML');
    end
end


if isempty(VarName)
    switch lower(Type)
        case 'by regions'
            VarName = 'school';
        case 'cell by cell'
            VarName = 'cell';
    end
end

end

function var_cell=get_variables(var_node)
nb_var=length(var_node.Children);
var_cell=cell(1,nb_var);
for i=1:nb_var
    var_cell{i}=get_att(var_node.Children(i),'name');
end
end

function node_cell=get_nodes(nodes_node)
nb_node=length(nodes_node.Children);
node_cell=cell(1,nb_node);
for inode=1:nb_node
    switch nodes_node.Children(inode).Name
        case '#comment'
        case 'node'
            node_cell{inode}=get_node_att(nodes_node.Children(inode));
            cond_child=get_childs(nodes_node.Children(inode),'condition');
            if ~isempty(cond_child)
                node_cell{inode}.Condition=cond_child.Data;
            end
            
            class_child=get_childs(nodes_node.Children(inode),'class');
            if ~isempty(class_child)
                node_cell{inode}.Class=class_child.Data;
                node_cell{inode}.Class_color = get_att(class_child,'rgb');
                if ~isempty(node_cell{inode}.Class_color)
                    node_cell{inode}.Class_color = str2double(strsplit(node_cell{inode}.Class_color,';'))/255;
                else
                    node_cell{inode}.Class_color = rand(1,3);
                end
            end
    end
end
node_cell(cellfun(@isempty,node_cell))=[];

end


function node_atts=get_node_att(node)
if isempty(node)
    node_atts=[];
    return;
end
nb_att=length(node.Attributes);
node_atts=[];
for j=1:nb_att
    node_atts.(node.Attributes(j).Name)=node.Attributes(j).Value;
end
end

function att_val=get_att(node,name)
    att_val=[];
    for iu=1:length(node.Attributes)
        if strcmpi(node.Attributes(iu).Name,name)
        att_val=node.Attributes(iu).Value;
        end
    end
end

function childs=get_childs(node,name)
    childs=[];
    for iu=1:length(node.Children)
        if strcmpi(node.Children(iu).Name,name)
        childs=[childs node.Children(iu)];
        end
    end
end
