function db_to_params = translate_db_to_params_cell()

db_to_params = {...
    {'parameters_frequency_start','FrequencyStart'},...
    {'parameters_frequency_end','FrequencyEnd'},...
    {'parameters_pulse_slope','Slope'},...
    {'parameters_power','TransmitPower'},...
    {'parameters_pulse_length','PulseLength'},...
    {'parameters_pulse_mode','Mode'}
    };