function [layers,id_rem] = open_NETCDF4_file_stdalone(Filename_cell,varargin)
id_rem = [];
layers = layer_cl.empty();

p = inputParser;

if ~iscell(Filename_cell)
    Filename_cell = {Filename_cell};
end

if ~iscell(Filename_cell)
    Filename_cell = {Filename_cell};
end

if isempty(Filename_cell)
    return;
end


def_path_m = fullfile(tempdir,'data_echo');

if ischar(Filename_cell)
    def_gps_only_val = 0;
else
    def_gps_only_val = zeros(1,numel(Filename_cell));
end

addRequired(p,'Filename_cell',@(x) ischar(x)||iscell(x));
addParameter(p,'PathToMemmap',def_path_m,@ischar);
addParameter(p,'Calibration',[]);
addParameter(p,'Frequencies',[]);
addParameter(p,'Channels',{});
addParameter(p,'GPSOnly',def_gps_only_val);
addParameter(p,'block_len',[],@(x) isempty(x)||x>0);
addParameter(p,'load_bar_comp',[]);

parse(p,Filename_cell,varargin{:});


nb_files = numel(Filename_cell);
load_bar_comp = p.Results.load_bar_comp;
block_len=p.Results.block_len;

if ~isempty(load_bar_comp)
    set(load_bar_comp.progress_bar, 'Minimum',0, 'Maximum',nb_files,'Value',0);
end

layers(length(Filename_cell)) = layer_cl();
id_rem = [];

%     f=figure();
%     ax = axes(f);
%     imh = imagesc(ax,0);
for uu = 1:nb_files

    if ~isempty(load_bar_comp)
        str_disp=sprintf('Opening File %d/%d : %s',uu,nb_files,Filename_cell{uu});
        load_bar_comp.progress_bar.setText(str_disp);
    end

    Filename = Filename_cell{uu};
    if ~isfile(Filename)
        id_rem = union(id_rem,uu);
        continue;
    end

    finfo = h5info(Filename);

    Groups_name  = {finfo.Groups(:).Name};
    idx_g_top  = find(contains(Groups_name,'Top-level'));
    idx_g_env  = find(contains(Groups_name,'Environment'));
    idx_g_sonar  = find(contains(Groups_name,'Sonar'));
    idx_g_platform  = find(contains(Groups_name,'Platform'));

    dataset_top = {};
    dataset_platform = {};
    dataset_env = {};

    data_top = [];
    if ~isempty(idx_g_top)
        dataset_top = {finfo.Groups(idx_g_top).Attributes(:).Name};
        for uid = 1:numel(dataset_top)
            data_top.(dataset_top{uid}) = finfo.Groups(idx_g_top).Attributes(uid).Value;
        end
    end

    data_env = [];
    if ~isempty(idx_g_env)
        dataset_env = {finfo.Groups(idx_g_env).Datasets(:).Name};
        for uid = 1:numel(dataset_env)
            data_env.(dataset_env{uid}) = h5read(Filename,sprintf('%s/%s',finfo.Groups(idx_g_env).Name,dataset_env{uid}));
        end
    end

    env_data_obj = env_data_cl();
    env_data_obj.Depth = data_env.water_depth;
    env_data_obj.SoundSpeed = data_env.sound_speed_indicative;


    data_platform = [];
    if ~isempty(idx_g_platform)
        dataset_platform = {finfo.Groups(idx_g_platform).Datasets(:).Name};
        for uid = 1:numel(dataset_platform)
            data_platform.(dataset_platform{uid}) = h5read(Filename,sprintf('%s/%s',finfo.Groups(idx_g_platform).Name,dataset_platform{uid}));
        end
    end

    sonar_group_names = {};
    nb_sonar_groups = 0;

    if ~isempty(idx_g_sonar)
        nb_sonar_groups = numel(finfo.Groups(idx_g_sonar).Groups);
        sonar_group_names = {finfo.Groups(idx_g_sonar).Groups(:).Name};
        %     {'backscatter_i'               }
        %     {'backscatter_r'               }
        %     {'beam'                        }
        %     {'beam_direction_x'            }
        %     {'beam_direction_y'            }
        %     {'beam_direction_z'            }
        %     {'beam_stabilisation'          }
        %     {'beam_type'                   }
        %     {'beamwidth_receive_major'     }
        %     {'beamwidth_receive_minor'     }
        %     {'beamwidth_transmit_major'    }
        %     {'beamwidth_transmit_minor'    }
        %     {'equivalent_beam_angle'       }
        %     {'gain_correction'             }
        %     {'non_quantitative_processing' }
        %     {'ping_time'                   }
        %     {'receiver_sensitivity'        }
        %     {'sample'                      }
        %     {'sample_interval'             }
        %     {'sample_time_offset'          }
        %     {'time'                        }
        %     {'time_varied_gain'            }
        %     {'transducer_gain'             }
        %     {'transmit_bandwidth'          }
        %     {'transmit_duration_equivalent'}
        %     {'transmit_duration_nominal'   }
        %     {'transmit_frequency_start'    }
        %     {'transmit_frequency_stop'     }
        %     {'transmit_power'              }
        %     {'transmit_source_level'       }
        %     {'transmit_type'               }
    end

    trans_obj_tot=[];

    att_sonar_names = {finfo.Groups(idx_g_sonar).Attributes(:).Name};

    att_sonar = [];
    for uid = 1:numel(att_sonar_names)
        att_sonar.(att_sonar_names{uid}) = finfo.Groups(idx_g_sonar).Attributes(uid).Value;
    end

    time_platform  = datenum(1601, 1, 1, 0, 0, double(data_platform.time1)/1e9);
    switch att_sonar.sonar_model

        case 'FCV-38'
            lat_deg = sign(data_platform.latitude).*floor(abs(data_platform.latitude));
            lat_min_dec = (data_platform.latitude-lat_deg)/0.6;
            data_platform.latitude =lat_deg+lat_min_dec;

            lon_deg = sign(data_platform.longitude).*floor(abs(data_platform.longitude));
            lon_min_dec = (data_platform.longitude-lon_deg)/0.6;
            data_platform.longitude = lon_deg+lon_min_dec;
    end

    gps_data_tot = gps_data_cl(...
        'Lat',data_platform.latitude,...
        'Long',data_platform.longitude,...
        'Speed',data_platform.speed_ground,...
        'Time',time_platform);

    att_data_tot =attitude_nav_cl(...
        'Heading',data_platform.heading,...
        'Pitch',data_platform.pitch,...
        'Roll', data_platform.roll,...
        'Time',time_platform);

    for itr = 1:nb_sonar_groups

        dataset_names  = {finfo.Groups(idx_g_sonar).Groups(itr).Datasets(:).Name};
        idx_data_keep = ~ismember(dataset_names,{'backscatter_r' 'backscatter_i'});
        dataset_names  = dataset_names(idx_data_keep);

        dataset_sizes  = {finfo.Groups(idx_g_sonar).Groups(itr).Datasets(:).ChunkSize};
        dataset_sizes = dataset_sizes(idx_data_keep);

        %along is major, across is minor

        data_sonar_to_import = {...
            'beam' ...
            'transmit_frequency_start'...
            'transmit_frequency_stop' ...
            'transmit_duration_nominal' ...
            'transmit_duration_equivalent' ...
            'sample_interval' ...
            'transmit_power' ...
            'sample' ...
            'beam_type' ...
            'transducer_gain' ...
            'transmit_power' ...
            'transmit_source_level' ...
            'ping_time' ...
            'sample_time_offset'};

        data_sonar_to_import_partially = {...
            'beam_type' ...
            'beamwidth_transmit_major' ...
            'beamwidth_transmit_minor' ...
            'beamwidth_receive_major' ...
            'beamwidth_receive_minor' ...
            'equivalent_beam_angle' ...
            'transmit_type'};

        read_all = false && ~isdeployed();

        data_sonar = [];


        for uid = 1:numel(dataset_names)
            if ismember(dataset_names{uid},data_sonar_to_import)||read_all
                data_sonar.(dataset_names{uid}) = h5read(Filename,sprintf('%s/%s',sonar_group_names{itr},dataset_names{uid}));
            elseif ismember(dataset_names{uid},data_sonar_to_import_partially)
                if numel(dataset_sizes{uid})==1
                    data_sonar.(dataset_names{uid}) = h5read(Filename,sprintf('%s/%s',sonar_group_names{itr},dataset_names{uid}),1,1);
                else
                    data_sonar.(dataset_names{uid}) = h5read(Filename,sprintf('%s/%s',sonar_group_names{itr},dataset_names{uid}),ones(size(dataset_sizes{uid})),[dataset_sizes{uid}(1) 1]);
                end
            elseif ~isdeployed()
                fprintf('Sonar Datasets not imported from NETCDF file: %s\n', dataset_names{uid});
            end
        end


        att_names = {finfo.Groups(idx_g_sonar).Groups(itr).Attributes(:).Name};

        for uid = 1:numel(att_names)
            data_sonar.(att_names{uid}) = finfo.Groups(idx_g_sonar).Groups(itr).Attributes(uid).Value;
        end

        ite_read = false;

        backscatter_r = h5read(Filename,sprintf('%s/%s',sonar_group_names{itr},'backscatter_r'));

        if ~ite_read
            backscatter_i = h5read(Filename,sprintf('%s/%s',sonar_group_names{itr},'backscatter_i'));
        end

        nb_samples_per_pings = cellfun(@numel,backscatter_r);
        nb_pings  = size(nb_samples_per_pings,2);

        nb_samples = max(nb_samples_per_pings,[],'all','omitnan');

        params_obj = params_cl(nb_pings,1);
        params_obj.BeamNumber = 1;

        params_obj.Frequency  = mean(data_sonar.transmit_frequency_start+data_sonar.transmit_frequency_stop)/2;
        params_obj.FrequencyStart  = mean(data_sonar.transmit_frequency_start);
        params_obj.FrequencyEnd  = mean(data_sonar.transmit_frequency_stop);
        params_obj.BeamAngleAlongship = zeros(1,nb_pings);
        params_obj.BeamAngleAthwartship = zeros(1,nb_pings);
        params_obj.ChannelMode = ones(1,nb_pings);
        params_obj.PulseLength = data_sonar.transmit_duration_nominal(:)';
        params_obj.TeffCompPulseLength = data_sonar.transmit_duration_nominal(:)';
        %params_obj.TeffPulseLength = data_sonar.transmit_duration_equivalent(:)';
        %params_obj.BandWidth = data_sonar.transmit_bandwidth(:)';
        params_obj.PulseForm;
        params_obj.SampleInterval = data_sonar.sample_interval(:)';
        %params_obj.Slope = nan(1,nb_pings);
        params_obj.TransmitPower  = (data_sonar.transmit_power(:)');

        if all(params_obj.TransmitPower == 0)
            params_obj.TransmitPower = 3.5*1e3*ones(size(params_obj.TransmitPower));
            print_errors_and_warnings([],'Warning','Transmit power not stored in .nc file, we will use 3.5kW per default, but this data is not usable quantitatively');
        end

        config_obj = config_cl();

        config_obj.EthernetAddress = '';
        config_obj.IPAddress = '';
        config_obj.SerialNumber = att_sonar.sonar_serial_number;
        config_obj.TransceiverName = sprintf('%s_%s',att_sonar.sonar_manufacturer,att_sonar.sonar_model);
        config_obj.TransceiverNumber = itr;
        config_obj.TransceiverSoftwareVersion = 0;
        config_obj.TransceiverType = att_sonar.sonar_model;
        config_obj.ChannelID = sprintf('%s_%s_%s',att_sonar.sonar_manufacturer,att_sonar.sonar_model,att_sonar.sonar_serial_number);
        config_obj.ChannelNumber = itr;
        config_obj.MaxTxPowerTransceiver = 0;
        config_obj.PulseLength = mean(data_sonar.transmit_duration_nominal);
        config_obj.AngleOffsetAlongship = 0;
        config_obj.AngleOffsetAthwartship = 0;


        switch data_sonar.beam_type{1}
            case 'split_aperture'
                config_obj.BeamType = 'split-beam';
            otherwise
                config_obj.BeamType = 'single-beam';
        end


        config_obj.BeamWidthAlongship = sqrt(mean(data_sonar.beamwidth_transmit_major(3:4))*mean(data_sonar.beamwidth_receive_major(3:4)));
        config_obj.BeamWidthAthwartship = sqrt(mean2(data_sonar.beamwidth_transmit_minor(1:2))*mean(data_sonar.beamwidth_receive_minor(1:2)));
        config_obj.SoundSpeedNominal = env_data_obj.SoundSpeed;

        config_obj.EquivalentBeamAngle = estimate_eba(config_obj.BeamWidthAlongship,config_obj.BeamWidthAthwartship);

        config_obj.Frequency = min(params_obj.Frequency);
        config_obj.FrequencyMaximum = min(min(params_obj.FrequencyStart),min(params_obj.FrequencyEnd));
        config_obj.FrequencyMinimum = max(max(params_obj.FrequencyStart),max(params_obj.FrequencyEnd));

        config_obj.Gain = data_sonar.transducer_gain(1);
        config_obj.Impedance = 1000;
        config_obj.Ztrd = 75;
        config_obj.MaxTxPowerTransducer = 4000;
        config_obj.SaCorrection = 0;
        config_obj.TransducerName = sprintf('Transducer_%s_%s_%s',att_sonar.sonar_manufacturer,att_sonar.sonar_model,att_sonar.sonar_serial_number);
        config_obj.XML_string;
        config_obj.Cal_FM;
        config_obj.TransducerAlphaX = data_platform.MRU_rotation_x;
        config_obj.TransducerAlphaY = data_platform.MRU_rotation_y;
        config_obj.TransducerAlphaZ = data_platform.MRU_rotation_z;
        config_obj.TransducerMounting = 'Hull';
        config_obj.TransducerOffsetX = data_platform.transducer_offset_x;
        config_obj.TransducerOffsetY = data_platform.transducer_offset_y;
        config_obj.TransducerOffsetZ = data_platform.transducer_offset_z;
        config_obj.TransducerOrientation = 'Downward-looking';
        config_obj.TransducerSerialNumber = att_sonar.sonar_serial_number;
        config_obj.Version = att_sonar.sonar_software_version;
        config_obj.EsOffset = 0;
        config_obj.NbQuadrants = numel(data_sonar.beam);
        config_obj.RXArrayShape = 'flat';
        config_obj.TXArrayShape = 'flat';


        time_tr = datenum(1601, 1, 1, 0, 0, double(data_sonar.ping_time)/1e9);


        [~,curr_filename,~]=fileparts(tempname);
        curr_data_name_t=fullfile(p.Results.PathToMemmap,curr_filename);
        nb_samples = max(nb_samples_per_pings,[],'all');
        nb_pings = size(nb_samples_per_pings,2);
        [nb_samples_s,~,bid] = unique(max(nb_samples_per_pings,[],1));
        nb_pings_s = nan(1,numel(nb_samples_s));

        for iui = 1:numel(nb_samples_s)
            nb_pings_s(iui) = sum(max(nb_samples_per_pings,[],1) == nb_samples_s(iui));
        end

        ac_data_temp = ac_data_cl('SubData',[],...
            'Nb_samples', nb_samples_s,...
            'Nb_pings',   nb_pings,...
            'BlockId', bid',...
            'Nb_beams',   ones(size(nb_samples_s)),...
            'MemapName',  curr_data_name_t);

        ac_data_temp.init_sub_data('power','DefaultValue',0);
        ac_data_temp.init_sub_data('acrossangle','DefaultValue',0);
        ac_data_temp.init_sub_data('alongangle','DefaultValue',0);


        if isempty(block_len)
            block_len=get_block_len(max(nb_samples)*20,'cpu');
        end

        num_ite = ceil(nb_pings/block_len);

        switch data_sonar.transmit_type{1}
            case {'LFM' 'HFM' 'FM'}
                mm = 'FM';
                ac_data_temp.init_sub_data('powerunmatched','DefaultValue',0);
                ac_data_temp.init_sub_data('y_real','DefaultValue',0);
                ac_data_temp.init_sub_data('y_imag','DefaultValue',0);
            otherwise
                mm = 'CW';
        end
        trans_obj=transceiver_cl('Data',ac_data_temp,...
            'Time',time_tr,...
            'Config',config_obj,...
            'Mode',mm,...
            'Params',params_obj);

        trans_obj.Config.SounderType = sprintf('Split-beam (%s)',att_sonar.sonar_manufacturer);

        %num_ite = 1;
        switch att_sonar.sonar_model

            case 'FCV-38'
                L0 =  0.1469;
                a = 0.1795;

                trans_obj.Config.Gain = 0;

                TR_G = 24 + data_sonar.transducer_gain(1,:)+10*log10(params_obj.TransmitPower/3500);

                lambda  = env_data_obj.SoundSpeed./params_obj.Frequency;

                AG = 10*log10(params_obj.TransmitPower.*lambda.^2/(16*pi^2));

                trans_obj.Config.EquivalentBeamAngle = 10*log10(5.78/(2*pi*params_obj.Frequency(1)/env_data_obj.SoundSpeed*a).^2);

                trans_obj.Config.AngleSensitivityAlongship   = 1/(env_data_obj.SoundSpeed/(2*pi*params_obj.Frequency(1)*L0));
                trans_obj.Config.AngleSensitivityAthwartship = 1/(env_data_obj.SoundSpeed/(2*pi*params_obj.Frequency(1)*L0));

                if ~isempty(load_bar_comp)
                    set(load_bar_comp.progress_bar, 'Minimum',0, 'Maximum',num_ite,'Value',0);
                end
                idx_ping_tot = 1:nb_pings;

                for ui = 1:num_ite

                    idx_ping_t = idx_ping_tot((ui-1)*block_len+1:min(ui*block_len,nb_pings));
                    nb_samples_tmp = max(nb_samples_per_pings(:,idx_ping_t),[],1);
                    id_sep = find(abs(diff(nb_samples_tmp))>0);

                    if isempty(id_sep)
                        id_start = idx_ping_t(1);
                        id_end = idx_ping_t(end);
                    else

                        id_start = idx_ping_t([1 id_sep+1]);
                        id_end = idx_ping_t([id_sep numel(idx_ping_t)]);

                    end

                    for iit = 1:numel(id_start)
                        idx_ping = id_start(iit):id_end(iit);

                        if ite_read
                            backscatter_i = h5read(Filename,sprintf('%s/%s',sonar_group_names{itr},'backscatter_i'),[1 idx_ping(1)],[size(backscatter_r,1) numel(idx_ping)]);
                            data_tmp.comp_sig_1=(cell2mat(backscatter_r(1,:))+1i*cell2mat(backscatter_i(1,:)));
                            data_tmp.comp_sig_2=(cell2mat(backscatter_r(2,:))+1i*cell2mat(backscatter_i(2,:)));
                            data_tmp.comp_sig_3=(cell2mat(backscatter_r(3,:))+1i*cell2mat(backscatter_i(3,:)));
                            data_tmp.comp_sig_4=(cell2mat(backscatter_r(4,:))+1i*cell2mat(backscatter_i(4,:)));
                        else
                            data_tmp.comp_sig_1=(cell2mat(backscatter_r(1,idx_ping))+1i*cell2mat(backscatter_i(1,idx_ping)));
                            data_tmp.comp_sig_2=(cell2mat(backscatter_r(2,idx_ping))+1i*cell2mat(backscatter_i(2,idx_ping)));
                            data_tmp.comp_sig_3=(cell2mat(backscatter_r(3,idx_ping))+1i*cell2mat(backscatter_i(3,idx_ping)));
                            data_tmp.comp_sig_4=(cell2mat(backscatter_r(4,idx_ping))+1i*cell2mat(backscatter_i(4,idx_ping)));
                        end

                        %                 stbd =s1;
                        %                 port =s2;
                        %                 fore=s3;
                        %                 aft =s4;

                        switch mm
                            case 'FM'

                                [sim_pulse,y_tx_matched,t_pulse]=trans_obj.get_pulse();
                                Np=numel(y_tx_matched);
                                data_tmp  = structfun(@(x) circshift(x,-Np,1),data_tmp,'un',0);
                                ff=fieldnames(data_tmp);
                                for uif=1:numel(ff)
                                    data_tmp.(ff{uif})(end-Np:end,:)=0;
                                end

                            case 'CW'
                                y_tx_matched = [];

                        end

                        trans_obj.Config.NbQuadrants=sum(contains(fieldnames(data_tmp),'comp_sig'));

                        A_square  = 16*(((real(data_tmp.comp_sig_1)+real(data_tmp.comp_sig_2)).^2+(imag(data_tmp.comp_sig_1)+imag(data_tmp.comp_sig_2)).^2))/(2^32-1).^2 ;
                        power =  (A_square/2).* db2pow(AG(idx_ping))./ db2pow(TR_G(idx_ping));

                        if ~isempty(y_tx_matched)
                            ac_data_temp.replace_sub_data_v2(power,'powerunmatched','idx_ping',idx_ping);
                            y = data_tmp.comp_sig_1 + data_tmp.comp_sig_2;%Just a wild guess to make it somehow work for FCV-38
                            trans_obj.Data.replace_sub_data_v2(real(y),'y_real','idx_ping',idx_ping)
                            trans_obj.Data.replace_sub_data_v2(imag(y),'y_imag','idx_ping',idx_ping)
                            data_tmp=match_filter_data(data_tmp,y_tx_matched,0);

                            A_square  = 16*(((real(data_tmp.comp_sig_1)+real(data_tmp.comp_sig_2)).^2+(imag(data_tmp.comp_sig_1)+imag(data_tmp.comp_sig_2)).^2))/(2^32-1).^2 ;
                            power =  (A_square/2).* db2pow(AG(idx_ping))./ db2pow(TR_G(idx_ping));
                        end

                        alongphi= angle(data_tmp.comp_sig_3.*conj(data_tmp.comp_sig_4));
                        acrossphi = angle(data_tmp.comp_sig_1.*conj(data_tmp.comp_sig_2));

                        AlongAngle=asind(alongphi/trans_obj.Config.AngleSensitivityAthwartship)-trans_obj.Config.AngleOffsetAlongship;
                        AcrossAngle=asind(acrossphi/trans_obj.Config.AngleSensitivityAthwartship)-trans_obj.Config.AngleOffsetAthwartship;

                        %A = 4*sqrt(abs(s1+s2))/(2^32-1));

                        ac_data_temp.replace_sub_data_v2(power,'power','idx_ping',idx_ping);
                        ac_data_temp.replace_sub_data_v2(AcrossAngle,'acrossangle','idx_ping',idx_ping);
                        ac_data_temp.replace_sub_data_v2(AlongAngle,'alongangle','idx_ping',idx_ping);
                    end
                    if ~isempty(load_bar_comp)
                        set(load_bar_comp.progress_bar, 'Minimum',0, 'Maximum',num_ite,'Value',ui);
                    end
                end
            otherwise
                warndlg_perso('Sounder not supported.','Sounder %s not supported yet. Please contact the developers, send them a file and they will add it.',att_sonar.sonar_model);
                id_rem = union(id_rem,uu);
                continue;
        end



        [~,range_t]=trans_obj(itr).compute_soundspeed_and_range(env_data_obj);
        trans_obj.set_transceiver_range(range_t);

        trans_obj.set_pulse_Teff();
        trans_obj.set_pulse_comp_Teff();
        trans_obj.set_absorption(data_env.absorption_indicative);
        trans_obj_tot = [trans_obj_tot trans_obj];
    end
    layers(uu)=layer_cl('Filename',{Filename},...
        'Filetype','NETCDF4',...
        'Transceivers',trans_obj_tot,...
        'GPSData',gps_data_tot,...
        'AttitudeNav',att_data_tot,...
        'EnvData',env_data_obj);

    if ~isempty(load_bar_comp)
        set(load_bar_comp.progress_bar, 'Minimum',0, 'Maximum',length(Filename_cell),'Value',uu);
    end
end
