
function lim_att =get_lim_config_att()
lim_att=   {'Xaxes_current'
            'Xaxes'
            'Fieldnames'
            'Cmap'
            'DispSecFreqsWithOffset'
            'Caxes'
            'Grid_x'
            'Grid_y'
            'YDir'};
