function data = get_data_from_das_netcdf(folder,fnames)

data = cell(1,numel(fnames));

for ui = 1:numel(fnames)
    
    tmp = dir(fullfile(folder,fnames{ui}));
    
    filenames =  fullfile(folder,{tmp(~[tmp(:).isdir]).name});
    

    for uif = 1 :numel(filenames)
        filename  = filenames{uif};
        finfo = ncinfo(filename);
        
        for uivar = 1:numel(finfo.Variables)
            d_tmp.(finfo.Variables(uivar).Name)  = ncread(filename,finfo.Variables(uivar).Name);
            switch finfo.Variables(uivar).Name
                case 'time'
                    d_tmp.(finfo.Variables(uivar).Name)= datetime(d_tmp.(finfo.Variables(uivar).Name),'ConvertFrom','excel');
            end
            
            if uif>1
                data{ui}.(finfo.Variables(uivar).Name) = [data{ui}.(finfo.Variables(uivar).Name);d_tmp.(finfo.Variables(uivar).Name)];
            else
                data{ui} = d_tmp;
            end
        end         
    end
    clear d_tmp
end