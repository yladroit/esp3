function gps_data=get_ping_data_from_db(filenames,freq_ori)

if isempty(filenames)
    gps_data={[]};
    return;
end

if~iscell(filenames)
    filenames ={filenames};
end

idata=1;

gps_data=cell(1,length(filenames));

for ip=1:length(filenames)
    try
        if isfolder(filenames{ip})
            path_f=filenames{ip};
        else
            [path_f,file,ext]=fileparts(filenames{ip});
        end
        db_file=fullfile(path_f,'echo_logbook.db');
        
        if ~(exist(db_file,'file')==2)
            continue;
        end
        
        dbconn=connect_to_db(db_file);
        
        if isempty(freq_ori)
            try
                freq=dbconn.fetch(sprintf('SELECT Frequency  FROM ping_data WHERE instr(Filename,''%s'') >0 limit 1',file));
                freq = freq.Frequency;
            catch
                freq=[];
            end
        else
            freq=freq_ori;
        end
        
        if isempty(freq)
            continue;
        end
        
        if isfolder(filenames{ip})
            gps_data_f=dbconn.fetch('SELECT Lat,Long,Time FROM ping_data');
        else
            gps_data_f=dbconn.fetch(sprintf('SELECT Lat,Long,Time FROM ping_data WHERE instr(Filename, ''%s'') >0 AND Frequency = %f',file,freq));
        end
        
        
        if ~isempty(gps_data_f)
            lat=(gps_data_f.Lat);
            lon=(gps_data_f.Long);
            time=datenum(gps_data_f.Time,'yyyy-mm-dd HH:MM:SS');
            idx_nan=isnan(lat);
            gps_data{ip}=gps_data_cl('Lat',lat(~idx_nan),...,...
                'Long',lon(~idx_nan),...
                'Time',time(~idx_nan));
        end
        idata=idata+1;
        close(dbconn);
    catch err
        print_errors_and_warnings([],'error',err);
    end
end
end