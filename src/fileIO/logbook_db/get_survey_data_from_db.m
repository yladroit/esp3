function survey_data=get_survey_data_from_db(filenames)
survey_data={};
if isempty(filenames)
    
    return;
end

if~iscell(filenames)
    filenames ={filenames};
end

survey_data=cell(1,numel(filenames));

for ip=1:length(filenames)
    try
        if isfolder(filenames{ip})
            path_f=filenames{ip};
        else
            [path_f,file,~]=fileparts(filenames{ip});
        end
        
        db_file=fullfile(path_f,'echo_logbook.db');
        
        if ~(exist(db_file,'file')==2)
            continue;
        end
        
        idata=1;
        dbconn=connect_to_db(db_file);
        
        survey_data_db=dbconn.fetch('SELECT Voyage,SurveyName FROM survey ');

        if isempty(survey_data_db)
            voy = '';
            sname = '';
        else
            voy = survey_data_db.Voyage{1};
            sname = survey_data_db.SurveyName{1};
        end
        
        if ~isfolder(filenames{ip})
            sql_query = sprintf('SELECT Snapshot,Type,Stratum,Transect,StartTime,EndTime,Comment FROM logbook WHERE instr(Filename, ''%s'')>0',file);
            opts = databaseImportOptions(dbconn,sql_query);

            %opts.VariableTypes(ismember(opts.VariableNames,{'StartTime','EndTime'})) = {'datetime'};
            
            curr_file_data=dbconn.fetch(sql_query,opts);
            nb_data=size(curr_file_data,1);
            
            for id=1:nb_data
                survey_data{ip}{idata}=survey_data_cl('Voyage',voy,...
                    'SurveyName',sname,...
                    'Type',curr_file_data.Type{id},...
                    'Snapshot',curr_file_data.Snapshot(id),...
                    'Stratum',curr_file_data.Stratum{id},...
                    'Transect',curr_file_data.Transect(id),...
                        'StartTime',datenum(curr_file_data.StartTime(id)),...
                    'EndTime',datenum(curr_file_data.EndTime(id)));
                idata=idata+1;
            end
        else
            survey_data{ip}{1}=survey_data_cl('Voyage',voy,...,...
                'SurveyName',sname);
        end
        close(dbconn);
    catch err
        print_errors_and_warnings([],'error',err);
    end
end


end
