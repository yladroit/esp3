function output_struct=CFAR_wc_features_detection(trans_obj,varargin)

p = inputParser;

addRequired(p,'trans_obj',@(x) isa(x,'transceiver_cl'));
addParameter(p,'thr_sv',-70,@isnumeric);
addParameter(p,'N_2D',20,@isnumeric);
addParameter(p,'N_3D',50,@isnumeric);
addParameter(p,'AR_min',1,@isnumeric);
addParameter(p,'Rext_min',25,@isnumeric);
addParameter(p,'stdR_min',2,@isnumeric);
addParameter(p,'L',20,@isnumeric);
addParameter(p,'GC',4,@isnumeric);
addParameter(p,'DT',5,@isnumeric);
addParameter(p,'NBeams',1,@isnumeric);
addParameter(p,'NSamps',2,@isnumeric);
addParameter(p,'conn_2D',8,@isnumeric);
addParameter(p,'Gx',5,@isnumeric);
addParameter(p,'Gy',5,@isnumeric);
addParameter(p,'Gz',5,@isnumeric);
addParameter(p,'rm_specular',true,@islogical);

addParameter(p,'load_bar_comp',[]);
addParameter(p,'block_len',get_block_len(5,'cpu'),@(x) x>0);
addParameter(p,'reg_obj',region_cl.empty(),@(x) isa(x,'region_cl'));
parse(p,trans_obj,varargin{:});

output_struct.done =  false;
output_struct.feature_struct.E = [];
output_struct.feature_struct.N = [];
output_struct.feature_struct.H = [];
output_struct.feature_struct.zone = [];
output_struct.feature_struct.alongdist = [];
output_struct.feature_struct.acrossdist = [];
output_struct.feature_struct.idx_r = [];
output_struct.feature_struct.idx_beam = [];
output_struct.feature_struct.idx_ping = [];
output_struct.feature_struct.sv = [];
output_struct.feature_struct.id = [];

thr_sv = p.Results.thr_sv;
N_2D = p.Results.N_2D;
N_3D = p.Results.N_3D;
AR_min = p.Results.AR_min;
Rext_min = p.Results.Rext_min;
stdR_min = p.Results.stdR_min;
L = p.Results.L;
GC = p.Results.GC;
DT = p.Results.DT;
NBeams = p.Results.NBeams;
NSamps = p.Results.NSamps;
conn_2D = p.Results.conn_2D;
Gx = p.Results.Gx;
Gy = p.Results.Gy;
Gz = p.Results.Gz;

if isempty(p.Results.reg_obj)
    idx_r=1:length(trans_obj.get_transceiver_range());
    idx_ping_tot=1:length(trans_obj.get_transceiver_pings());
    reg_obj=region_cl('Name','Temp','Idx_r',idx_r,'Idx_ping',idx_ping_tot);
else
    reg_obj=p.Results.reg_obj;
end

idx_ping_tot=reg_obj.Idx_ping;
idx_r=reg_obj.Idx_r;

%trans_obj_range = trans_obj.get_transceiver_range(idx_r);

load_bar_comp = p.Results.load_bar_comp;
up_bar=~isempty(load_bar_comp);

if isempty(p.Results.reg_obj)
    reg_obj=region_cl('Idx_r',idx_r,'Idx_ping',idx_ping_tot);
else
    reg_obj=p.Results.reg_obj;
end

nb_Beams = max(trans_obj.Data.Nb_beams,[],'all','omitnan');

block_size=max(min(ceil(p.Results.block_len/numel(idx_r)/nb_Beams),numel(idx_ping_tot)),min(500,numel(idx_ping_tot)));

num_ite=ceil(numel(idx_ping_tot)/block_size);

if up_bar
    p.Results.load_bar_comp.progress_bar.setText('CFAR WC Feature detection');
    set(p.Results.load_bar_comp.progress_bar, 'Minimum',0, 'Maximum',num_ite, 'Value',0);
end
[T,Np] = trans_obj.get_pulse_length();
min_specular_sample_buffer = 10*max(Np,[],'all','omitnan');

for ui=1:num_ite

    idx_ping=idx_ping_tot((ui-1)*block_size+1:min(ui*block_size,numel(idx_ping_tot)));
    idx_ping  = idx_ping(1)-L:idx_ping(end)+L;
    idx_ping(idx_ping<1) = [];
    idx_ping(idx_ping>numel(trans_obj.get_transceiver_pings())) = [];

    reg_temp=region_cl('Name','Temp','Idx_r',idx_r,'Idx_ping',idx_ping);
    [sv_SPB,idx_r,idx_ping,bad_data_mask,bad_trans_vec,~,below_bot_mask,~]=trans_obj.get_data_from_region(reg_temp,'field','sv','alt_fields',{'wc_data','img_intensity'},'intersect_only',1,...
        'regs',reg_obj);

    bot_s = trans_obj.get_bottom_idx(idx_ping);
    bot_s(bot_s == 1 ) = nan;
    min_bot  = min(bot_s,[],3,"omitnan");
    BeamAngleAthwartship=trans_obj.get_params_value('BeamAngleAthwartship',idx_ping,[]);

    if p.Results.rm_specular
        specular_mask = idx_r >= min_bot-min_specular_sample_buffer./cosd(BeamAngleAthwartship) & idx_r <= min_bot+min_specular_sample_buffer./cosd(BeamAngleAthwartship);
    else
        specular_mask =  false(size(sv_SPB));
    end


    sv_SPB_lin = db2pow_perso(sv_SPB);
    sv_SPB_lin(sv_SPB<thr_sv | specular_mask) = 0;
    %sv_SPB_lin(smooth3(sv_SPB<thr_sv,'box',[2*NSamps+1 1 2*NBeams+1]) == 0) = 0;
    sv_SPB_lin(below_bot_mask) = 0;

    detection_mask = CFAR_detector(sv_SPB_lin,'L',L,'GC',GC,'DT',DT,'load_bar_comp',load_bar_comp);


    if isempty(detection_mask)
        continue;
    end

    [ClusterID,ClusterN] = ping_clustering(detection_mask,'NBeams',NBeams,'NSamps',NSamps,'N_2D',N_2D,'conn',conn_2D,'load_bar_comp',load_bar_comp);

    detection_mask_pc = (detection_mask & ClusterN > N_2D);
    %detection_mask_pc = (detection_mask & ClusterN > N_2D);


    if isempty(detection_mask_pc)
        continue;
    end

      
    [E,N,H,zone,no_nav,ald,acd]=trans_obj.get_xxx_ENH('data_to_pos',{'WC'},'idx_ping',idx_ping,'idx_r',idx_r,...
        'comp_angle',false,...
        'yaw_comp',false,...
        'roll_comp',false,...
        'pitch_comp',false,...
        'detection_mask',detection_mask_pc);

    [nb_samples,nb_pings,~] = size(detection_mask_pc);

    ii = find(detection_mask_pc);
    iBeam = ceil(ii/(nb_samples*nb_pings));
    iPing = ceil((ii-(iBeam-1)*nb_samples*nb_pings)/nb_samples);
    iSample  = ii-(iBeam-1)*nb_samples*nb_pings-(iPing-1)*nb_samples;
    
    iPing = iPing + idx_ping(1) - 1;
    iSample = iSample + idx_r(1) - 1;
    
    sv = sv_SPB(ii);
    
    debug_disp = false & ~isdeployed;

    if debug_disp

        if ~isempty(ald)
            loc1 = [acd',ald',-H.WC'];
            ptCloud = pointCloud(loc1);
            figure()
            pcshow(ptCloud)

            figure()
            pcshow(ptCloud.Location,ClusterID(ii))
            title('Point Cloud Clusters')
        end

%         detection_mask_3D = false(size(detection_mask_pc));
%         detection_mask_3D(ii) = true;

        curr_disp = curr_state_disp_cl();

        ff = new_echo_figure([],...
            'Name','CFAR detector',...
            'tag','wc_fan',...
            'UiFigureBool',true);

        uigl = uigridlayout(ff,[3 1]);

        wc_fan = create_wc_fan('wc_fig',uigl,'curr_disp',curr_disp);
        wc_fan_cfar = create_wc_fan('wc_fig',uigl,'curr_disp',curr_disp);
        wc_fan_cfar_pc = create_wc_fan('wc_fig',uigl,'curr_disp',curr_disp);
        %wc_fan_cfar_pc_3D = create_wc_fan('wc_fig',uigl,'curr_disp',curr_disp);

        wc_fan.gl_ax.Tag = 'Original data';
        wc_fan_cfar.gl_ax.Tag = 'After CFAR detector';
        wc_fan_cfar_pc.gl_ax.Tag = 'After CFAR detector and within-ping clustering';
        %wc_fan_cfar_pc_3D.gl_ax.Tag = 'After CFAR detector, withing-ping clustering and 3D clustering';

        for ip = 1:size(sv_SPB,2)
            disp_ping_wc_fan(wc_fan,trans_obj,'ip',idx_ping(ip),'curr_disp',curr_disp,'idx_r',idx_r);
            disp_ping_wc_fan(wc_fan_cfar,trans_obj,'ip',idx_ping(ip),'mask',detection_mask(:,ip,:),'curr_disp',curr_disp,'idx_r',idx_r);
            disp_ping_wc_fan(wc_fan_cfar_pc,trans_obj,'ip',idx_ping(ip),'mask',detection_mask_pc(:,ip,:),'curr_disp',curr_disp,'idx_r',idx_r);
%             disp_ping_wc_fan(wc_fan_cfar_pc_3D,trans_obj,'ip',idx_ping(ip),'mask',detection_mask_3D(:,ip,:),'curr_disp',curr_disp,'idx_r',idx_r);
            pause(0.1);
        end
    end

    output_struct.feature_struct.E = [output_struct.feature_struct.E E.WC];
    output_struct.feature_struct.N = [output_struct.feature_struct.N N.WC];
    output_struct.feature_struct.H = [output_struct.feature_struct.H H.WC];
    output_struct.feature_struct.zone = [output_struct.feature_struct.zone zone.WC];
    output_struct.feature_struct.alongdist = [output_struct.feature_struct.alongdist ald];
    output_struct.feature_struct.acrossdist = [output_struct.feature_struct.acrossdist acd];
    output_struct.feature_struct.idx_r = [output_struct.feature_struct.idx_r iSample'];
    output_struct.feature_struct.idx_beam = [output_struct.feature_struct.idx_beam iBeam'];
    output_struct.feature_struct.idx_ping = [output_struct.feature_struct.idx_ping iPing'];
    output_struct.feature_struct.sv = [output_struct.feature_struct.sv sv'];

    if up_bar
        p.Results.load_bar_comp.progress_bar.setText('CFAR WC Feature detection');
        set(p.Results.load_bar_comp.progress_bar, 'Minimum',0, 'Maximum',num_ite, 'Value',ui);
    end

end

x = output_struct.feature_struct.acrossdist;
y = output_struct.feature_struct.alongdist;
z = -output_struct.feature_struct.H;

[ClusterID,ClusterN] = data_3D_clustering(x,y,z,'Gx',Gx,'Gy',Gy,'Gz',Gz,'N_3D',N_3D,'load_bar_comp',load_bar_comp);
%        if ~isempty(x)
%             loc1 = [x',y',z'];
%             ptCloud = pointCloud(loc1,output_struct.feature_struct.sv);
%             figure()
%             pcshow(ptCloud)
%             figure()
%             pcshow(ptCloud.Location,ClusterID)
%             title('Point Cloud Clusters')
%         end
R = sqrt(x.^2+z.^2);
cids = unique(ClusterID);

Rext = nan(size(cids));
Zext = nan(size(cids));
Hext = nan(size(cids));
stdR = nan(size(cids));

for uic = 1:numel(cids)
    id = ClusterID == cids(uic);
    Rc = R(id);
    Rext(uic)  = range(Rc);
    Zext(uic) = range(z(id));
    Hext(uic) = range(x(id));
    stdR(uic) =  std(sqrt(x(id).^2+z(id).^2));
end

AR = Zext./Hext;

cids_rem = cids(Rext<Rext_min | AR<AR_min | stdR < stdR_min);

id_rem = ClusterN<N_3D | ismember(ClusterID,cids_rem);

[~,~,tmp]  =unique(ClusterID);
output_struct.feature_struct.id = tmp';

ff = fieldnames(output_struct.feature_struct);

for uif = 1:numel(ff)
    output_struct.feature_struct.(ff{uif})(id_rem) = [];
end

[~,~,tmp] = unique(output_struct.feature_struct.id);
output_struct.feature_struct.id = tmp';

%        if ~isempty(x)
%             loc1 = [output_struct.feature_struct.acrossdist',output_struct.feature_struct.alongdist',-output_struct.feature_struct.H'];
%             ptCloud = pointCloud(loc1);
%             figure()
%             pcshow(ptCloud)
%             figure()
%             pcshow(ptCloud.Location,output_struct.feature_struct.id)
%             title('Point Cloud Clusters')
%         end

trans_obj.set_FT(output_struct.feature_struct)

output_struct.done = true;