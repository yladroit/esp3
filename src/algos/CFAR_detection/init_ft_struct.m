function feature_struct = init_ft_struct(nb_features)

feature_struct = struct(...
    'E',nan(1,nb_features),...
    'N',nan(1,nb_features),...
    'H',nan(1,nb_features),...
    'zone',nan(1,nb_features),...
    'alongdist',nan(1,nb_features),...
    'acrossdist',nan(1,nb_features),...
    'idx_r',nan(1,nb_features),...
    'idx_ping',nan(1,nb_features),...
    'idx_beam',nan(1,nb_features),...
    'sv',nan(1,nb_features),...
    'id',nan(1,nb_features));
