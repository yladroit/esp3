function [ClusterID,ClusterN] = data_3D_clustering(x,y,z,varargin)

p = inputParser;
addRequired(p,'x',@isnumeric);
addRequired(p,'y',@isnumeric);
addRequired(p,'z',@isnumeric);
addParameter(p,'Gx',10,@(x) x>0);
addParameter(p,'Gy',10,@(x) x>0);
addParameter(p,'Gz',10,@(x) x>0);
addParameter(p,'N_3D',20,@(x) x>0);
addParameter(p,'load_bar_comp',[]);

parse(p,x,y,z,varargin{:});

if ~isempty(p.Results.load_bar_comp)
    p.Results.load_bar_comp.progress_bar.setText('3D clustering...');
end

y = y*p.Results.Gx/p.Results.Gy;
z = z*p.Results.Gx/p.Results.Gz;


loc1 = [x(:),y(:),z(:)];
ptCloud = pointCloud(loc1);
[ClusterID,numClusters] = pcsegdist(ptCloud,p.Results.Gx) ;
ClusterN = ones(size(ClusterID),'uint32');
cids = unique(ClusterID);

for uic = 1:numel(cids)
    idc = ClusterID == cids(uic);
    ClusterN(idc) = sum(idc);
end


end

