function [ClusterID,ClusterN] = ping_clustering(detection_mask,varargin)

p = inputParser;
addRequired(p,'detection_mask',@islogical);
addParameter(p,'NBeams',1,@(x) x>0);
addParameter(p,'NSamps',2,@(x) x>0);
addParameter(p,'N_2D',10,@(x) x>0);
addParameter(p,'conn',8,@isnumeric);
addParameter(p,'load_bar_comp',[]);

parse(p,detection_mask,varargin{:});

if ~isempty(p.Results.load_bar_comp)
    p.Results.load_bar_comp.progress_bar.setText('Ping clustering...');
end

NBeams = p.Results.NBeams;
NSamps = p.Results.NSamps;

%detection_mask = smooth3(detection_mask,'box',[2*NSamps+1 1 2*NBeams+1])>0;
detection_mask = permute(detection_mask,[1 3 2]);

[nb_samples,nb_beams,nb_pings] = size(detection_mask);



ClusterID = zeros(size(detection_mask),'uint32');
CC = bwconncomp(detection_mask,p.Results.conn);
candidates = CC.PixelIdxList;
candidates(cellfun(@numel,candidates)<p.Results.N_2D) = [];
candidates = cellfun(@transpose,candidates,'UniformOutput',false);
ii = [candidates{:}];

ClusterID(ii) = 1:numel(ii);

iPing = ceil(ii/(nb_samples*nb_beams));
iBeam = ceil((ii-(iPing-1)*nb_samples*nb_beams)/nb_samples);
iSample  = ii-(iPing-1)*nb_samples*nb_beams-(iBeam-1)*nb_samples;

ClusterN = zeros(size(detection_mask),'uint32');

pings = min(iPing):max(iPing);

if ~isempty(p.Results.load_bar_comp)
    set(p.Results.load_bar_comp.progress_bar, 'Minimum',0, 'Maximum',numel(pings), 'Value',0);
end

idmin = 0;

for ip = 1:numel(pings)
    idp = find(iPing == pings(ip));

    if ~isempty(p.Results.load_bar_comp)
        set(p.Results.load_bar_comp.progress_bar,'Value',ip);
    end

    if isempty(idp)
        continue;
    end

    sub_cluster = ClusterID(ii(idp));

    for uicl = idp
        id = abs(iBeam(idp)-iBeam(uicl))<=NBeams & abs(iSample(idp)-iSample(uicl))<=NSamps;
        sub_cluster(ismember(sub_cluster,sub_cluster(id))) = min(sub_cluster(id));
    end

    [cids,~,ic] = unique(sub_cluster);
    
    ClusterID(ii(idp)) = ic+idmin;

    idmin = max(ic+idmin);

    for uic = 1:numel(cids)
        idc = sub_cluster == cids(uic);
        ClusterN(ii(idp(idc))) = sum(idc);
    end

end

ClusterN = permute(ClusterN,[1 3 2]);
ClusterID = permute(ClusterID,[1 3 2]);

end

