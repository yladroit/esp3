classdef echo_3D_cl < handle

    properties
        ax_3D matlab.ui.control.UIAxes
        light_h matlab.graphics.primitive.Light
        curtain_surf_h matlab.graphics.chart.primitive.Surface
        scatter_features_h matlab.graphics.chart.primitive.Scatter
        bathy_surf_h matlab.graphics.chart.primitive.Surface
        bot_line_h matlab.graphics.chart.primitive.Line
        trans_line_h matlab.graphics.chart.primitive.Line
        text_h matlab.graphics.primitive.Text
        colorbar_h matlab.graphics.illustration.ColorBar
        vert_ex_slider_h matlab.ui.control.Slider
        vert_ex_edit_h matlab.ui.control.NumericEditField
        linked_props = [];
        ax_linked_props = [];

    end

    methods
        function obj = echo_3D_cl(varargin)
            p = inputParser;

            addParameter(p,'cmap','ek60',@ischar);
            addParameter(p,'disp_colorbar',true,@islogical);
            addParameter(p,'disp_grid','on',@(x) ismember(x,{'off','on'}));
            addParameter(p,'FaceAlpha','flat',@ischar);
            addParameter(p,'ZDir','reverse');
            addParameter(p,'AlphaDataMapping','direct',@ischar);
            addParameter(p,'FontSize',9,@isnumeric);
            addParameter(p,'offset',true,@islogical);
            parse(p,varargin{:});

            fields=fieldnames(p.Results);
            for ifi=1:numel(fields)
                if isprop(obj,fields{ifi})
                    obj.(fields{ifi})= p.Results.(fields{ifi});
                end
            end

            [cmap,col_ax,col_lab,col_grid,col_bot,col_txt,col_tracks]=init_cmap(p.Results.cmap);

            [~,parent_h] = obj.init_echo_3D_fig();

            control_layout = uigridlayout(parent_h,[3,2]);
            control_layout.RowHeight = {'fit','fit','1x'};
            control_layout.ColumnWidth = {'3x','1x'};
            control_layout.Layout.Row = 1;
            control_layout.Layout.Column = 1;
            control_layout.BackgroundColor = col_ax;

            tmp_label = uilabel(control_layout);
            tmp_label.HorizontalAlignment = 'left';
            tmp_label.Layout.Row = 1;
            tmp_label.Layout.Column = 1;
            tmp_label.Text = 'Vertical exageration';

            obj.vert_ex_slider_h = uislider('Parent',control_layout,'Value',10,'Limits',[0 500]);
            obj.vert_ex_slider_h.Layout.Row = 2;
            obj.vert_ex_slider_h.Layout.Column = 1;
            obj.vert_ex_slider_h.ValueChangedFcn = @obj.change_vert_exa;
            obj.vert_ex_slider_h.ValueChangingFcn = @obj.update_vert_ex_edit_h;

            obj.vert_ex_edit_h = uieditfield(control_layout, 'numeric');
            obj.vert_ex_edit_h.Limits = [0 500];
            obj.vert_ex_edit_h.ValueDisplayFormat = '%.1f';
            obj.vert_ex_edit_h.Layout.Row = 2;
            obj.vert_ex_edit_h.Layout.Column = 2;
            obj.vert_ex_edit_h.Value = 10;
            obj.vert_ex_edit_h.ValueChangedFcn = @obj.change_vert_exa;

            main_figure = get_esp3_prop('main_figure');



            obj.ax_3D=uiaxes(...
                'Parent',parent_h,...
                'Color',col_ax,...
                'GridColor',col_grid,...
                'MinorGridColor',col_grid,...
                'XColor',col_lab,...
                'YColor',col_lab,...
                'ZColor',col_lab,...
                'FontSize',p.Results.FontSize,...
                'Box','on',...
                'SortMethod','childorder',...
                'XGrid',p.Results.disp_grid,...
                'YGrid',p.Results.disp_grid,...
                'ZGrid',p.Results.disp_grid,...
                'XMinorGrid','off',...
                'YMinorGrid','off',...
                'ZMinorGrid','off',...
                'GridLineStyle','--',...
                'MinorGridLineStyle',':',...
                'NextPlot','add',...
                'ZDir',p.Results.ZDir,...
                'ClippingStyle','rectangle',...
                'Toolbar',[],...
                'Colormap',cmap,...
                'Tag',p.Results.cmap);



            obj.light_h = light(obj.ax_3D,'Position',[-1 0 1],'Style','local');
            %lightangle(VZ,-45,30);

            %obj.ax_3D.Interactions = [];
            axtoolbar(obj.ax_3D,{'restoreview' 'rotate' 'pan' 'zoomin' 'zoomout'});
            obj.ax_3D.Interactions = [rotateInteraction zoomInteraction];
            enableDefaultInteractivity(obj.ax_3D);


            echo_fig = obj.get_parent_figure();

            if ~isempty(main_figure)
                echo_fig.Alphamap = main_figure.Alphamap;
            end

            %obj.light_h = light(obj.ax_3D,'Visible','off');

            parent_h.BackgroundColor = col_ax;
            obj.ax_3D.Layout.Row = 1;
            obj.ax_3D.Layout.Column = 2;

            obj.colorbar_h=colorbar(obj.ax_3D,...
                'PickableParts','none',...
                'fontsize',p.Results.FontSize-2,...
                'Color',col_lab);


            view(obj.ax_3D,[45 45]);
            obj.colorbar_h = colorbar(obj.ax_3D);
            obj.colorbar_h.Color = col_lab;

            obj.ax_3D.ZAxis.TickLabelFormat = '%.0fm';
            obj.ax_3D.YAxis.TickLabelInterpreter = 'tex';
            obj.ax_3D.YAxis.TickLabelFormat = '%.2f^{o}';
            obj.ax_3D.XAxis.TickLabelInterpreter = 'tex';
            obj.ax_3D.XAxis.TickLabelFormat = '%.2f^{o}';

            view(obj.ax_3D,[45 45]);


            if (will_it_work(parent_h,'9.8',true)||will_it_work(parent_h,'',false))
                obj.colorbar_h.UIContextMenu=[];
            end

            if ~isempty(main_figure)
                axes_panel_comp=getappdata(main_figure,'Axes_panel');
                obj.linked_props=linkprop([main_figure echo_fig],'Alphamap');
                if ~isempty(axes_panel_comp)&&isvalid(axes_panel_comp.echo_obj.main_ax)
                    obj.linked_props = [obj.linked_props linkprop([axes_panel_comp.echo_obj.main_ax obj.ax_3D],'Clim')];
                end
            end

        end

        function parent_fig = get_parent_figure(obj)
            if ~isempty(obj)
                parent_fig = ancestor(obj.ax_3D,'figure');
            else
                parent_fig =  matlab.ui.Figure.empty;
            end
        end

        function [s_h,b_h,tr_h,te_h,bathy_h,scatter_h] = get_graphic_handles(obj,tag)
            s_h  = obj.curtain_surf_h;
            b_h  = obj.bot_line_h;
            tr_h  = obj.trans_line_h;
            te_h  = obj.text_h;
            bathy_h  = obj.bathy_surf_h;
            scatter_h = obj.scatter_features_h;

            if ~isempty(obj.curtain_surf_h)
                s_h = obj.curtain_surf_h(strcmpi({obj.curtain_surf_h(:).Tag},tag));
            end
            if ~isempty(obj.bot_line_h)
                b_h = obj.bot_line_h(strcmpi({obj.bot_line_h(:).Tag},tag));
            end
            if ~isempty(obj.trans_line_h)
                tr_h = obj.trans_line_h(strcmpi({obj.trans_line_h(:).Tag},tag));
            end
            if ~isempty(obj.text_h)
                te_h = obj.text_h(strcmpi({obj.text_h(:).Tag},tag));
            end

            if ~isempty(obj.scatter_features_h)
                scatter_h = obj.scatter_features_h(strcmpi({obj.scatter_features_h(:).Tag},tag));
            end

            if ~isempty(obj.bathy_surf_h)
                bathy_h = obj.bathy_surf_h(strcmpi({obj.bathy_surf_h(:).Tag},tag));
            end
        end

        function add_bathy(obj,lay_obj,varargin)

            p = inputParser;
            addRequired(p,'obj',@(x) isa(x,'echo_3D_cl'));
            addRequired(p,'lay_obj',@(x) isa(x,'layer_cl'));
            addParameter(p,'tag','bathy',@ischar);
            addParameter(p,'grid_size',0,@(x) @isnumeric);
            addParameter(p,'surv_data',survey_data_cl.empty,@(x) isa(x,'survey_data_cl'))
            addParameter(p,'load_bar_comp',[]);
            parse(p,obj,lay_obj,varargin{:});



            E_tot = [];
            N_tot = [];
            H_tot = [];
            zone = [];
            for uilay = 1:numel(lay_obj)
                trans_obj = lay_obj(uilay).Transceivers(1);
                if trans_obj.ismb
                    trans_depth=trans_obj.get_transceiver_top_depth();
                    [E_tmp,N_tmp,H_tmp,zone_tmp] = trans_obj.get_xxx_ENH('data_to_pos',{'bottom'});
                    H_tmp = H_tmp.bottom +trans_depth;
                    zone = [zone; zone_tmp.bottom(:)];
                    E_tot =[E_tot ; E_tmp.bottom(:)];
                    N_tot =[N_tot ; N_tmp.bottom(:)];
                    H_tot =[H_tot ; H_tmp(:)];

                end
            end

            if isempty(E_tot)
                print_errors_and_warnings([],'log','This layer does not contain MBES bathy data');
                return;
            end

            id_rem = isnan(E_tot.*N_tot);
            N_tot(id_rem) = [];
            E_tot(id_rem) = [];
            H_tot(id_rem) = [];
            zone(id_rem) = [];

            zone_u = unique(zone);

            for iz = 1:numel(zone_u)
                idz = zone == zone_u(iz);
                E = E_tot(idz);
                N = N_tot(idz);
                H = H_tot(idz);


                tag = sprintf('%s_%d',p.Results.tag,zone_u(iz));

                [curtain_surf_h_f,bot_h_f,trans_h_f,text_h_f,bathy_h] = obj.get_graphic_handles(tag);

                grid_size = p.Results.grid_size;

                if grid_size ==0
                    [k,area] = convhull(E,N);
                    sounding_per_sqm =  (numel(E)/area);
                    grid_size = 4/sqrt(sounding_per_sqm);
%                     p = polyshape(E(k),N(k));
                end

                grid_meth  = 'accumaray';
                

                switch grid_meth
                    case'accumaray'
                        x_idx = round(E/grid_size);
                        x_idx = x_idx-min(x_idx,[],'all')+1;
                        y_idx = round(N/grid_size);
                        y_idx = y_idx-min(y_idx,[],'all')+1;
                        H_grid = accumarray([y_idx(:) x_idx(:)],H(:),[], @(x)mean(x,'all','omitnan'),nan);

                        [Ny,Nx] = size(H_grid);

                        [E_grid,N_grid] = meshgrid(min(E,[],'all','omitnan')+(0:Nx-1)*grid_size,min(N,[],'all','omitnan')+(0:Ny-1)*grid_size);
                    otherwise
                        Nx = numel(unique(round(E/grid_size)));
                        Ny = numel(unique(round(N/grid_size)));

                        [E_grid,N_grid] = meshgrid(min(E,[],'all','omitnan')+(0:Nx-1)*grid_size,min(N,[],'all','omitnan')+(0:Ny-1)*grid_size);

                        F = scatteredInterpolant(E,N,H,'nearest','none') ;
                        
                        H_grid = F(E_grid,N_grid);

                end
                [lat_grid,lon_grid] = utm2ll(E_grid(:),N_grid(:),zone_u(iz));

                lat_grid = reshape(lat_grid,size(E_grid));
                lon_grid = reshape(lon_grid,size(E_grid));

                if isempty(bathy_h)
%                     T = delaunay(lat_grid,lon_grid);
                    bathy_surf_h_temp = surf(obj.ax_3D,lat_grid,lon_grid,H_grid,...
                        'Tag',tag);
                    bathy_surf_h_temp.AlphaDataMapping = 'scaled';
                    bathy_surf_h_temp.LineStyle = 'none';
                    bathy_surf_h_temp.EdgeColor = 'none';
                    bathy_surf_h_temp.BackFaceLighting = 'unlit';
                    bathy_surf_h_temp.FaceColor =  [0.6 0.6 0.6];
                    bathy_surf_h_temp.FaceAlpha =  0.8;
                    bathy_surf_h_temp.EdgeAlpha =  0.8;

                    obj.bathy_surf_h = [obj.bathy_surf_h bathy_surf_h_temp];

                else
                    bathy_h.XData = lat_grid;
                    bathy_h.YData = lon_grid;
                    bathy_h.ZData = H_grid;
                    bathy_h.AlphaData = double(~isnan(H_grid));
                end

            end

            obj.ax_3D.ZLim =  [min(H_grid(:),[],'omitnan') max(H_grid(:),[],'omitnan')].*[0.9 1.1];

            obj.change_vert_exa(obj.vert_ex_slider_h,[]);

        end

        function add_feature(obj,trans_obj,varargin)
            
            check_ref = @(ref) ~isempty(strcmpi(ref,{'Surface','Bottom','Transducer'}));

             p = inputParser;
            addRequired(p,'obj',@(x) isa(x,'echo_3D_cl'));
            addRequired(p,'trans_obj',@(x) isa(x,'transceiver_cl'));
            addParameter(p,'tag','',@ischar);
            addParameter(p,'beam_angular_limit',[-inf inf],@(x) @isnumeric);
            addParameter(p,'rangeBounds',[0 inf],@(x) @isnumeric);
            addParameter(p,'refRangeBounds',[-inf inf],@(x) @isnumeric);
            addParameter(p,'depthBounds',[-inf inf],@isnumeric);
            addParameter(p,'cax',[-75 -38],@isnumeric);
            addParameter(p,'main_figure',[],@(x) isempty(x)||ishandle(x));
            addParameter(p,'fieldname','sv',@ischar);
            addParameter(p,'Unique_ID',generate_Unique_ID([]),@ischar);
            addParameter(p,'Ref','Surface',check_ref);
            addParameter(p,'intersect_only',false,@(x) isnumeric(x)||islogical(x));
            addParameter(p,'idx_regs',[],@isnumeric);
            addParameter(p,'regs',region_cl.empty(),@(x) isa(x,'region_cl'));
            addParameter(p,'select_reg','all',@ischar);
            addParameter(p,'surv_data',survey_data_cl.empty,@(x) isa(x,'survey_data_cl'))
            addParameter(p,'keep_bottom',false,@(x) isnumeric(x)||islogical(x));
            addParameter(p,'block_len',[],@(x) x>0 || isempty(x));
            addParameter(p,'load_bar_comp',[]);
            parse(p,obj,trans_obj,varargin{:});
            
            if isempty(trans_obj.FT.idx_r)
                return;
            end

  
            if isempty(p.Results.surv_data)
                surv_data  = survey_data_cl();
                surv_data.StartTime = trans_obj.Time(1);
                surv_data.EndTime = trans_obj.Time(end);
            else
                surv_data = p.Results.surv_data;
            end

            if isempty(p.Results.tag)
                tag_s = sprintf('%s_%s_%s',trans_obj.Config.ChannelID,datestr(surv_data.StartTime,'yyyymmddHHMMSS'),datestr(surv_data.EndTime,'yyyymmddHHMMSS'));
            else
                tag_s = p.Results.tag;
            end

            [curtain_surf_h_f,bot_h_f,trans_h_f,text_h_f,~,scat_h] = obj.get_graphic_handles(tag_s);

            [Lat_t,Lon_t] = utm2ll(trans_obj.FT.E(:),trans_obj.FT.N(:),trans_obj.FT.zone(:));
            
            switch p.Results.fieldname
                case 'feature_sv'
                    data_disp = trans_obj.FT.sv;
                case 'feature_id'
                    data_disp = trans_obj.FT.id;
            end
                   
            cax  = p.Results.cax;

            caxis(obj.ax_3D,cax);

            if isempty(scat_h)
                scat_h_temp = scatter3(obj.ax_3D,...
                    Lat_t,...
                    Lon_t,....
                    trans_obj.FT.H,...
                    15,...
                    data_disp,'Tag',tag_s,'Marker','.','MarkerFaceAlpha',0.8,'MarkerEdgeAlpha',0.8);

                obj.scatter_features_h = [obj.scatter_features_h scat_h_temp];
            else
                scat_h.XData = Lat_t;
                scat_h.YData = Lon_t;
                scat_h.ZData = trans_obj.FT.H;
                scat_h.CData = data_disp;
            end

            obj.ax_3D.ZLim =  [min(trans_obj.FT.H(:),[],'omitnan') max(trans_obj.FT.H(:),[],'omitnan')].*[0.9 1.1];

            obj.change_vert_exa(obj.vert_ex_slider_h,[]);


        end


        function add_surface(obj,trans_obj,varargin)

            check_ref = @(ref) ~isempty(strcmpi(ref,{'Surface','Bottom','Transducer'}));

            p = inputParser;
            addRequired(p,'obj',@(x) isa(x,'echo_3D_cl'));
            addRequired(p,'trans_obj',@(x) isa(x,'transceiver_cl'));
            addParameter(p,'tag','',@ischar);
            addParameter(p,'beam_angular_limit',[-5 5],@(x) @isnumeric);
            addParameter(p,'rangeBounds',[0 inf],@(x) @isnumeric);
            addParameter(p,'refRangeBounds',[-inf inf],@(x) @isnumeric);
            addParameter(p,'depthBounds',[-inf inf],@isnumeric);
            addParameter(p,'cax',[-75 -38],@isnumeric);
            addParameter(p,'main_figure',[],@(x) isempty(x)||ishandle(x));
            addParameter(p,'fieldname','sv',@ischar);
            addParameter(p,'Unique_ID',generate_Unique_ID([]),@ischar);
            addParameter(p,'Ref','Surface',check_ref);
            addParameter(p,'intersect_only',false,@(x) isnumeric(x)||islogical(x));
            addParameter(p,'idx_regs',[],@isnumeric);
            addParameter(p,'regs',region_cl.empty(),@(x) isa(x,'region_cl'));
            addParameter(p,'select_reg','all',@ischar);
            addParameter(p,'surv_data',survey_data_cl.empty,@(x) isa(x,'survey_data_cl'))
            addParameter(p,'keep_bottom',false,@(x) isnumeric(x)||islogical(x));
            addParameter(p,'block_len',[],@(x) x>0 || isempty(x));
            addParameter(p,'load_bar_comp',[]);
            parse(p,obj,trans_obj,varargin{:});


            [cmap,col_ax,col_lab,col_grid,col_bot,col_txt,col_tracks]=init_cmap(obj.ax_3D.Tag);

            beam_angular_limit = p.Results.beam_angular_limit;

            if isempty(p.Results.surv_data)
                surv_data  = survey_data_cl();
                surv_data.StartTime = trans_obj.Time(1);
                surv_data.EndTime = trans_obj.Time(end);
            else
                surv_data = p.Results.surv_data;
            end

            if isempty(p.Results.tag)
                tag_s = sprintf('%s_%s_%s',trans_obj.Config.ChannelID,datestr(surv_data.StartTime,'yyyymmddHHMMSS'),datestr(surv_data.EndTime,'yyyymmddHHMMSS'));
            else
                tag_s = p.Results.tag;
            end

            [curtain_surf_h_f,bot_h_f,trans_h_f,text_h_f,bathy_h] = obj.get_graphic_handles(tag_s);

            reg_wc = trans_obj.create_WC_region('Ref',p.Results.Ref,'t_min',surv_data.StartTime,'t_max',surv_data.EndTime);
        
            [Sv,idx_r,idx_ping,bad_data_mask,bad_trans_vec,intersection_mask,below_bot_mask,mask_from_st] =	trans_obj.get_data_from_region(reg_wc,...
                'rangeBounds',p.Results.rangeBounds,...
                'refRangeBounds',p.Results.refRangeBounds,...
                'depthBounds',p.Results.depthBounds,...
                'beamAngularLimit',beam_angular_limit,...
                'timeBounds',[surv_data.StartTime surv_data.EndTime],...
                'field',p.Results.fieldname,...
                'intersect_only',p.Results.intersect_only,...
                'idx_regs',p.Results.idx_regs,...
                'regs',p.Results.regs,...
                'select_reg',p.Results.select_reg,...
                'keep_bottom',p.Results.keep_bottom);

            is = find(~bad_trans_vec,1,'first');
            ie = find(~bad_trans_vec,1,'last');

            prc_thr = 100-max(log10(1e4/size(Sv,2)),1);
            dmax =  prctile(Sv,prc_thr,[2 3]);
            idx_last= numel(dmax) - find(flipud(dmax>-900),1)+1;
            if isempty(idx_last)
                idx_last = numel(idx_r);
            end

            Sv = Sv(1:idx_last,:,:);
            bad_data_mask = bad_data_mask(1:idx_last,:,:);
            intersection_mask = intersection_mask(1:idx_last,:,:);
            idx_r = idx_r(1:idx_last);
            below_bot_mask = below_bot_mask(1:idx_last,:,:);
            mask_from_st = mask_from_st(1:idx_last,:,:);

            if ~isempty(is)&&~isempty(ie)
                idx_ping = idx_ping(is:ie);
                bad_data_mask = bad_data_mask(:,is:ie);
                Sv = Sv(:,is:ie,:);
                intersection_mask = intersection_mask(:,is:ie);
                below_bot_mask = below_bot_mask(:,is:ie,:);
                mask_from_st = mask_from_st(:,is:ie);
                bad_trans_vec = bad_trans_vec(is:ie);
            end

            Sv(~intersection_mask) = nan;

            sub_p = ceil(numel(idx_ping)/2e3);
            sub_r = ceil(numel(idx_r)/2e3);

            index_r = ceil(idx_r/sub_r);
            index_r = index_r-min(index_r,[],'omitnan')+1;
            index_p = ceil(idx_ping/sub_p);
            index_p = index_p-min(index_p,[],'omitnan')+1;

            index_r_mat = repmat(index_r,1,numel(index_p),size(Sv,3));
            index_p_mat = repmat(index_p,numel(index_r),1,size(Sv,3));

            [~,ip] = unique(index_p);
            [~,ir] = unique(index_r);

            cax  = p.Results.cax;

            bad_data_mask(:,bad_trans_vec) = true;
            

            Sv_sub = accumarray([index_r_mat(~bad_data_mask) index_p_mat(~bad_data_mask)],Sv(~bad_data_mask),[numel(ir) numel(ip)],@(x) pow2db_perso(mean(db2pow_perso(x),'omitnan')),-999);

            below_bot_sub = below_bot_mask(ir+(ip'-1)*size(Sv,1));

            below_bot_sub = reshape(below_bot_sub,size(Sv_sub));
            bot_d = trans_obj.get_bottom_depth(idx_ping);
            trans_depth = trans_obj.get_transducer_depth(idx_ping);

            %alpha_map_fig=get(main_figure,'alphamap')%6 elts vector: first: empty, second: under clim(1), third: underbottom, fourth: bad trans, fifth regions, sixth normal]

            adata =ones(size(Sv_sub),'single')*6;
            adata(below_bot_sub) = 2;
            adata(Sv_sub<cax(1)) = 1;
            dd = trans_obj.get_transceiver_depth(idx_r(ir),idx_ping(ip));
            idx_r_min = find(any(Sv_sub>-180,2),1,'first');
            idx_r_max = find(any(Sv_sub>-180,2),1,'last');
            idx_r_new = idx_r_min:idx_r_max;

            if isempty(idx_r_new)
                return;
            end
             
            if trans_obj.ismb
                dd = squeeze(dd(:,:,ceil(size(dd,3)/2)));
            end

            dd = dd(idx_r_new,:); 
            Sv_sub = Sv_sub(idx_r_new,:);
            adata= adata(idx_r_new,:);

            if isempty(text_h_f)
                text_h_tmp = text(obj.ax_3D,trans_obj.GPSDataPing.Lat(idx_ping(1)),...
                    trans_obj.GPSDataPing.Long(idx_ping(1)),...
                    dd(1,1),surv_data.print_survey_data,...
                    'Color',col_lab,'Rotation',-90,'Interpreter','none','Tag',tag_s,'visible','off');
                obj.text_h = [obj.text_h text_h_tmp];
            else
                text_h_f.Position = [trans_obj.GPSDataPing.Lat(idx_ping(1)) trans_obj.GPSDataPing.Long(idx_ping(1)) mean(squeeze(trans_obj.get_transceiver_depth(idx_r(1),idx_ping(1))),'omitnan')];
                text_h_f.String = surv_data.print_survey_data;
            end

            if ~ismb(trans_obj)
                if isempty(bot_h_f)
                    bot_h_tmp = plot3(obj.ax_3D,trans_obj.GPSDataPing.Lat(idx_ping),trans_obj.GPSDataPing.Long(idx_ping),bot_d,'Color',col_bot,'Tag',tag_s);
                    obj.bot_line_h = [obj.bot_line_h bot_h_tmp];
                else
                    bot_h_f.XData = trans_obj.GPSDataPing.Lat(idx_ping);
                    bot_h_f.YData = trans_obj.GPSDataPing.Long(idx_ping);
                    bot_h_f.ZData = bot_d;
                end
            end

            if isempty(trans_h_f)
                trans_h_tmp = plot3(obj.ax_3D,...
                    trans_obj.GPSDataPing.Lat(idx_ping),...
                    trans_obj.GPSDataPing.Long(idx_ping),...
                    trans_depth,'Color',col_bot,'Tag',tag_s,'visible','off');
                obj.trans_line_h = [obj.trans_line_h trans_h_tmp];
            else
                trans_h_f.XData = trans_obj.GPSDataPing.Lat(idx_ping);
                trans_h_f.YData = trans_obj.GPSDataPing.Long(idx_ping);
                trans_h_f.ZData = trans_depth;
            end

            if isempty(curtain_surf_h_f)
                curtain_surf_h_temp = surf(obj.ax_3D,repmat(trans_obj.GPSDataPing.Lat(idx_ping(ip)),size(Sv_sub,1),1),...
                    repmat(trans_obj.GPSDataPing.Long(idx_ping(ip)),size(Sv_sub,1),1),....
                    dd,...
                    Sv_sub,'Tag',tag_s);
                curtain_surf_h_temp.FaceLighting = 'none';
                curtain_surf_h_temp.EdgeColor = 'none';
                curtain_surf_h_temp.LineStyle = 'none';
                curtain_surf_h_temp.FaceAlpha= 'flat';
                curtain_surf_h_temp.EdgeAlpha =  'flat';
                curtain_surf_h_temp.AlphaData = adata;
                obj.curtain_surf_h = [obj.curtain_surf_h curtain_surf_h_temp];
            else
                curtain_surf_h_f.XData = repmat(trans_obj.GPSDataPing.Lat(idx_ping(ip)),size(Sv_sub,1),1);
                curtain_surf_h_f.YData = repmat(trans_obj.GPSDataPing.Long(idx_ping(ip)),size(Sv_sub,1),1);
                curtain_surf_h_f.ZData = dd;
                curtain_surf_h_f.CData = Sv_sub;
                curtain_surf_h_f.AlphaData = adata;
            end

            caxis(obj.ax_3D,cax);

            obj.ax_3D.ZLim =  [min(dd(:),[],'omitnan') max(dd(:),[],'omitnan')].*[0.9 1.1];

            obj.change_vert_exa(obj.vert_ex_slider_h,[]);

        end

        function rem_surface(obj,trans_obj,varargin)

            p = inputParser;
            addRequired(p,'obj',@(x) isa(x,'echo_3D_cl'));
            addRequired(p,'trans_obj',@(x) isa(x,'transceiver_cl'));
            addParameter(p,'tag','',@ischar);
            addParameter(p,'surv_data',survey_data_cl.empty,@(x) isa(x,'survey_data_cl'))
            addParameter(p,'load_bar_comp',[]);
            parse(p,obj,trans_obj,varargin{:});

            if ~isempty(p.Results.tag)
                tag_s = p.Results.tag;
            else
                if isempty(p.Results.surv_data)
                    surv_data  = survey_data_cl();
                    surv_data.StartTime = trans_obj.Time(1);
                    surv_data.EndTime = trans_obj.Time(end);
                else
                    surv_data = p.Results.surv_data;
                end

                tag_s = sprintf('%s_%s_%s',trans_obj.Config.ChannelID,datestr(surv_data.StartTime,'yyyymmddHHMMSS'),datestr(surv_data.EndTime,'yyyymmddHHMMSS'));
            end

            [curtain_surf_h_f,bot_h_f,trans_h_f,text_h_f] = obj.get_graphic_handles(tag_s);
            delete(curtain_surf_h_f);
            delete(bot_h_f);
            delete(trans_h_f);
            delete(text_h_f);

            obj.clean_handles();

        end


        function rem_bathy(obj,trans_obj,varargin)

            p = inputParser;
            addRequired(p,'obj',@(x) isa(x,'echo_3D_cl'));
            addRequired(p,'trans_obj',@(x) isa(x,'transceiver_cl'));
            addParameter(p,'tag','',@ischar);
            addParameter(p,'load_bar_comp',[]);
            parse(p,obj,trans_obj,varargin{:});

            if ~isempty(p.Results.tag)
                tag_s = p.Results.tag;
            else

                tag_s = 'bathy';
            end

            [curtain_surf_h_f,bot_h_f,trans_h_f,text_h_f,bathy_h] = obj.get_graphic_handles(tag_s);
            delete(bathy_h);

            obj.clean_handles();

        end

        function clean_handles(obj)
            obj.curtain_surf_h(~isvalid(obj.curtain_surf_h))=[];
            obj.bot_line_h(~isvalid(obj.bot_line_h))=[];
            obj.text_h(~isvalid(obj.text_h))=[];
            obj.trans_line_h(~isvalid(obj.trans_line_h))=[];
            obj.bathy_surf_h(~isvalid(obj.bathy_surf_h))=[];

        end

        function [ff,parent_h] = init_echo_3D_fig(obj)
            ff=new_echo_figure(get_esp3_prop('main_figure'),'Units','Pixels',...
                'Name','3D view','Tag','3D view','WhichScreen','other','UiFigureBool',true,'CloseRequestFcn',@rm_3d_echo_obj);
            parent_h  = uigridlayout(ff,[1,2]);
            parent_h.ColumnWidth = {200,'1x'};
            function rm_3d_echo_obj(src,~)
                delete(obj);
                delete(src);
            end
        end

        function change_vert_exa(obj,src,evt)

            d = obj.ax_3D.DataAspectRatio;

            xx = mean(obj.ax_3D.XLim);
            yy = mean(obj.ax_3D.YLim);

            dx = diff(obj.ax_3D.XLim);
            dz = diff(obj.ax_3D.ZLim);


            switch class(src)
                case 'matlab.ui.control.NumericEditField'
                    vert_ex = obj.vert_ex_edit_h.Value;
                    obj.vert_ex_slider_h.Value = vert_ex;
                case 'matlab.ui.control.Slider'
                    vert_ex = obj.vert_ex_slider_h.Value;
                    obj.vert_ex_edit_h.Value = vert_ex;
                case 'double'
                    vert_ex = src;
                    obj.vert_ex_edit_h.Value = vert_ex;
                    obj.vert_ex_slider_h.Value = vert_ex;
            end

            if vert_ex>0

                d(3) = dx*(6378*1e3/180*pi)^2/dz/vert_ex;
                
                d(2) = 1;
                d(1) = d(2) * cosd(xx);

                daspect(obj.ax_3D,d);
            end
        end

        function update_vert_ex_edit_h(obj,src,evt)
            vert_ex = evt.Value;
            obj.vert_ex_edit_h.Value = vert_ex;
        end

        function delete(obj)
            delete(obj.linked_props);
            if isvalid(obj.ax_3D)
                delete(findall(obj.ax_3D));
                esp3_obj=getappdata(groot,'esp3_obj');
                esp3_obj.echo_3D_obj = echo_3D_cl.empty();
            end
        end

    end
end


