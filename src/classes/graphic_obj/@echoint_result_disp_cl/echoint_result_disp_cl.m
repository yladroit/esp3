classdef echoint_result_disp_cl < handle

    properties
        echo_disp_obj echo_disp_cl

        %         light_h matlab.graphics.primitive.Light
        %         curtain_surf_h matlab.graphics.chart.primitive.Surface
        %         scatter_features_h matlab.graphics.chart.primitive.Scatter
        %         bathy_surf_h matlab.graphics.chart.primitive.Surface
        %         bot_line_h matlab.graphics.chart.primitive.Line
        %         trans_line_h matlab.graphics.chart.primitive.Line
        %         text_h matlab.graphics.primitive.Text
        %         colorbar_h matlab.graphics.illustration.ColorBar
        %         vert_ex_slider_h matlab.ui.control.Slider
        %         vert_ex_edit_h matlab.ui.control.NumericEditField
        %         linked_props = [];
        %         ax_linked_props = [];

    end

    methods
        %         function obj = echo_3D_cl(varargin)
        %             p = inputParser;
        %
        %             addParameter(p,'cmap','ek60',@ischar);
        %             addParameter(p,'disp_colorbar',true,@islogical);
        %             addParameter(p,'disp_grid','on',@(x) ismember(x,{'off','on'}));
        %             addParameter(p,'FaceAlpha','flat',@ischar);
        %             addParameter(p,'ZDir','reverse');
        %             addParameter(p,'AlphaDataMapping','direct',@ischar);
        %             addParameter(p,'FontSize',9,@isnumeric);
        %             addParameter(p,'offset',true,@islogical);
        %             parse(p,varargin{:});
        %
        %             fields=fieldnames(p.Results);
        %             for ifi=1:numel(fields)
        %                 if isprop(obj,fields{ifi})
        %                     obj.(fields{ifi})= p.Results.(fields{ifi});
        %                 end
        %             end
        %
        %             [cmap,col_ax,col_lab,col_grid,col_bot,col_txt,col_tracks]=init_cmap(p.Results.cmap);
        %
        %             [~,parent_h] = obj.init_echo_3D_fig();
        %
        %             control_layout = uigridlayout(parent_h,[3,2]);
        %             control_layout.RowHeight = {'fit','fit','1x'};
        %             control_layout.ColumnWidth = {'3x','1x'};
        %             control_layout.Layout.Row = 1;
        %             control_layout.Layout.Column = 1;
        %             control_layout.BackgroundColor = col_ax;
        %
        %             tmp_label = uilabel(control_layout);
        %             tmp_label.HorizontalAlignment = 'left';
        %             tmp_label.Layout.Row = 1;
        %             tmp_label.Layout.Column = 1;
        %             tmp_label.Text = 'Vertical exageration';
        %
        %             obj.vert_ex_slider_h = uislider('Parent',control_layout,'Value',10,'Limits',[0 500]);
        %             obj.vert_ex_slider_h.Layout.Row = 2;
        %             obj.vert_ex_slider_h.Layout.Column = 1;
        %             obj.vert_ex_slider_h.ValueChangedFcn = @obj.change_vert_exa;
        %             obj.vert_ex_slider_h.ValueChangingFcn = @obj.update_vert_ex_edit_h;
        %
        %             obj.vert_ex_edit_h = uieditfield(control_layout, 'numeric');
        %             obj.vert_ex_edit_h.Limits = [0 500];
        %             obj.vert_ex_edit_h.ValueDisplayFormat = '%.1f';
        %             obj.vert_ex_edit_h.Layout.Row = 2;
        %             obj.vert_ex_edit_h.Layout.Column = 2;
        %             obj.vert_ex_edit_h.Value = 10;
        %             obj.vert_ex_edit_h.ValueChangedFcn = @obj.change_vert_exa;
        %
        %             main_figure = get_esp3_prop('main_figure');
        %
        %
        %
        %             obj.ax_3D=uiaxes(...
        %                 'Parent',parent_h,...
        %                 'Color',col_ax,...
        %                 'GridColor',col_grid,...
        %                 'MinorGridColor',col_grid,...
        %                 'XColor',col_lab,...
        %                 'YColor',col_lab,...
        %                 'ZColor',col_lab,...
        %                 'FontSize',p.Results.FontSize,...
        %                 'Box','on',...
        %                 'SortMethod','childorder',...
        %                 'XGrid',p.Results.disp_grid,...
        %                 'YGrid',p.Results.disp_grid,...
        %                 'ZGrid',p.Results.disp_grid,...
        %                 'XMinorGrid','off',...
        %                 'YMinorGrid','off',...
        %                 'ZMinorGrid','off',...
        %                 'GridLineStyle','--',...
        %                 'MinorGridLineStyle',':',...
        %                 'NextPlot','add',...
        %                 'ZDir',p.Results.ZDir,...
        %                 'ClippingStyle','rectangle',...
        %                 'Toolbar',[],...
        %                 'Colormap',cmap,...
        %                 'Tag',p.Results.cmap);
        %
        %
        %
        %             obj.light_h = light(obj.ax_3D,'Position',[-1 0 1],'Style','local');
        %             %lightangle(VZ,-45,30);
        %
        %             %obj.ax_3D.Interactions = [];
        %             axtoolbar(obj.ax_3D,{'restoreview' 'rotate' 'pan' 'zoomin' 'zoomout'});
        %             obj.ax_3D.Interactions = [rotateInteraction zoomInteraction];
        %             enableDefaultInteractivity(obj.ax_3D);
        %
        %
        %             echo_fig = obj.get_parent_figure();
        %
        %             if ~isempty(main_figure)
        %                 echo_fig.Alphamap = main_figure.Alphamap;
        %             end
        %
        %             %obj.light_h = light(obj.ax_3D,'Visible','off');
        %
        %             parent_h.BackgroundColor = col_ax;
        %             obj.ax_3D.Layout.Row = 1;
        %             obj.ax_3D.Layout.Column = 2;
        %
        %             obj.colorbar_h=colorbar(obj.ax_3D,...
        %                 'PickableParts','none',...
        %                 'fontsize',p.Results.FontSize-2,...
        %                 'Color',col_lab);
        %
        %
        %             view(obj.ax_3D,[45 45]);
        %             obj.colorbar_h = colorbar(obj.ax_3D);
        %             obj.colorbar_h.Color = col_lab;
        %
        %             obj.ax_3D.ZAxis.TickLabelFormat = '%.0fm';
        %             obj.ax_3D.YAxis.TickLabelInterpreter = 'tex';
        %             obj.ax_3D.YAxis.TickLabelFormat = '%.2f^{o}';
        %             obj.ax_3D.XAxis.TickLabelInterpreter = 'tex';
        %             obj.ax_3D.XAxis.TickLabelFormat = '%.2f^{o}';
        %
        %             view(obj.ax_3D,[45 45]);
        %
        %
        %             if (will_it_work(parent_h,'9.8',true)||will_it_work(parent_h,'',false))
        %                 obj.colorbar_h.UIContextMenu=[];
        %             end
        %
        %             if ~isempty(main_figure)
        %                 axes_panel_comp=getappdata(main_figure,'Axes_panel');
        %                 obj.linked_props=linkprop([main_figure echo_fig],'Alphamap');
        %                 if ~isempty(axes_panel_comp)&&isvalid(axes_panel_comp.echo_obj.main_ax)
        %                     obj.linked_props = [obj.linked_props linkprop([axes_panel_comp.echo_obj.main_ax obj.ax_3D],'Clim')];
        %                 end
        %             end
        %
        %         end

    end
end