%% load_echo_logbook_db.m
%
% TODO: write short description of function
%
%% Help
%
% *USE*
%
% TODO: write longer description of function
%
% *INPUT VARIABLES*
%
% * |layers_obj|: TODO: write description and info on variable
%
% *OUTPUT VARIABLES*
%
% NA
%
% *RESEARCH NOTES*
%
% TODO: write research notes
%
% *NEW FEATURES*
%
% * 2017-04-02: header (Alex Schimel).
% * YYYY-MM-DD: first version (Yoann Ladroit). TODO: complete date and comment
%
% *EXAMPLE*
%
% TODO: write examples
%
% *AUTHOR, AFFILIATION & COPYRIGHT*
%
% Yoann Ladroit, NIWA. Type |help EchoAnalysis.m| for copyright information.

%% Function
function load_echo_logbook_db(layers_obj)

incomplete=0;
[pathtofile,~]=layers_obj.get_path_files();
pathtofile=unique(pathtofile);

pathtofile(cellfun(@isempty,pathtofile))=[];

for ip=1:length(pathtofile)
    try
        fileN=fullfile(pathtofile{ip},'echo_logbook.db');
        
        if ~isfile(fileN)
            dbconn = initialize_echo_logbook_dbfile(pathtofile{ip},[],0);
        else
            dbconn = connect_to_db(fileN);
        end
        
        files_db=dbconn.fetch('SELECT Filename from logbook');
        
        close(dbconn);
        [ac_files,~]=list_ac_files(pathtofile{ip},1);
        
        if isempty(files_db.Filename)
            new_files = ac_files;
        else
            [new_files,~] = setdiff(ac_files,files_db.Filename);
        end

        if~isempty(new_files)
            ftypes=get_ftype_cell(cellfun(@(x) fullfile(pathtofile{ip},x),new_files,'UniformOutput',0));
            idx_rem=strcmpi(ftypes,'unknown');
            new_files(idx_rem)=[];
        end

        if ~isempty(new_files)
            incomplete=1;
            fprintf('%s incomplete, we''ll update it\n',fileN);
        end
        
    catch err
        print_errors_and_warnings([],'error',err);
    end
end

try
    if incomplete>0
        layers_obj.update_echo_logbook_dbfile('NewFilesOnly',true);
    else
        
    end
    
    layers_obj.add_survey_data_db(); 
catch err
    print_errors_and_warnings([],'error',err);
end