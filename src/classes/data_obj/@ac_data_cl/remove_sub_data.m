function remove_sub_data(data,fields)

if nargin<2
    fields=data.Fieldname;
end

if ~iscell(fields)
    fields={fields};
end

for ii=1:length(fields)
    fieldname=fields{ii};
    [idx,found]=find_field_idx(data,fieldname);
    
    if found==0
        continue;
    else
        fname=cell(1,length(data.SubData(idx).Memap));
        
        for icell=1:length(data.SubData(idx).Memap)
            if isprop(data.SubData(idx).Memap{icell},'Filename')||isfield(data.SubData(idx).Memap{icell},'Filename')
                fname{icell}=data.SubData(idx).Memap{icell}.Filename;
            else
                fname{icell} = '';
            end
        end
        
        %data.SubData(idx).delete();
        data.SubData(idx)=[];
        data.Type(idx)=[];
        data.Fieldname(idx)=[];
        
        for ic=1:length(fname)
            if isfile(fname{ic})
                delete(fname{ic});
            end
        end
    end
end
end
