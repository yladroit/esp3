function [E,N,H,zone,no_nav,Along_dist,Across_dist] = get_xxx_ENH(trans_obj,varargin)
p  =  inputParser;
nb_beams = max(trans_obj.Data.Nb_beams,[],'omitnan');
idx_beam_def =  1:nb_beams;
addRequired(p,'trans_obj',@(obj) isa(obj,'transceiver_cl'));
addParameter(p,'data_to_pos',{'bot'},@iscell);
addParameter(p,'roll_comp',~trans_obj.ismb,@islogical);
addParameter(p,'pitch_comp',~trans_obj.ismb,@islogical);
addParameter(p,'yaw_comp',~trans_obj.ismb,@islogical);
addParameter(p,'comp_angle',true,@islogical);
addParameter(p,'idx_ping',trans_obj.get_transceiver_pings(),@isnumeric);
addParameter(p,'idx_beam',idx_beam_def,@isnumeric);
addParameter(p,'idx_r',trans_obj.get_transceiver_samples(),@isnumeric);
addParameter(p,'detection_mask',[],@islogical);

parse(p,trans_obj,varargin{:});

E  =  [];
N  =  [];
H  =  [];
zone = [];
no_nav =  false;
comp_angle=p.Results.comp_angle;

if ~isempty(p.Results.idx_ping)
    idx_ping  =  p.Results.idx_ping;
else
    idx_ping  =  trans_obj.get_transceiver_pings();
end

if isempty(idx_ping)
    return;
end

if ~isempty(p.Results.idx_r)
    idx_r  =  p.Results.idx_r;
else
    idx_r  =  trans_obj.get_transceiver_samples();
end

if isempty(idx_r)
    return;
end

if ~isempty(p.Results.idx_beam)
    idx_beam  =  p.Results.idx_beam;
    nb_beams = numel(idx_beam);
else
    nb_beams = max(trans_obj.Data.Nb_beams,[],'omitnan');
    idx_beam =  1:nb_beams;
end

if isempty(idx_ping)
    return;
end

heading_geo = trans_obj.AttitudeNavPing.Heading(idx_ping);%TOFIX when there is no attitude data...

if p.Results.roll_comp
    pitch_geo = trans_obj.AttitudeNavPing.Pitch(idx_ping);
else
    pitch_geo = zeros(size(heading_geo));
end

if p.Results.roll_comp
    roll_geo = trans_obj.AttitudeNavPing.Roll(idx_ping);
else
    roll_geo = zeros(size(heading_geo));
end

if p.Results.yaw_comp
    yaw_geo = trans_obj.AttitudeNavPing.Yaw(idx_ping);
else
    yaw_geo = zeros(size(heading_geo));
end

heave_geo = trans_obj.AttitudeNavPing.Heave(idx_ping);

if ~isempty(trans_obj.GPSDataPing.Lat)
    lat = trans_obj.GPSDataPing.Lat(idx_ping);
    long = trans_obj.GPSDataPing.Long(idx_ping);
    %dist = trans_obj.GPSDataPing.Dist(idx_ping);
else
    lat = zeros(size(idx_ping));
    long = zeros(size(idx_ping));
    %dist = zeros(size(idx_ping));
end

if isempty(heading_geo)||all(isnan(heading_geo))||all(heading_geo == -999)
    disp('No Heading Data, we''ll pretend to go north');
    heading_geo = zeros(size(idx_ping));
end

if all(lat  ==  0)
    warning('No navigation data, we''ll pretend to go at 10 knots following our heading....');
    dt   =  mean(diff(trans_obj.AttitudeNavPing.Time(idx_ping)),'all','omitnan')*24;%average ping rate in hours
    dist = ((1:numel(trans_obj.AttitudeNavPing.Time(idx_ping)))-1)*dt*10*1852;
    easting = dist.*sind(heading_geo);%
    northing = dist.*cosd(heading_geo);
    no_nav =  true;
    zone_tt = -60;
else
    [easting,northing,zone_tt] = ll2utm(lat,long);
end

if isscalar(zone_tt)
    zone_tt = repmat(zone_tt,1,size(northing,2));
end

if isempty(pitch_geo)
    disp('No motion Data, we''ll pretend everythings flat');
    roll_geo=zeros(size(northing));
    pitch_geo=zeros(size(northing));
    heave_geo=zeros(size(northing));
    yaw_geo=zeros(size(northing));
end

if size(heading_geo,1)>1
    heading_geo=heading_geo';
end

if size(roll_geo,1)>1
    roll_geo=roll_geo';
    pitch_geo=pitch_geo';
    heave_geo=heave_geo';
    yaw_geo=yaw_geo';
end


pitch_geo(isnan(pitch_geo))=0;
roll_geo(isnan(roll_geo))=0;
heave_geo(isnan(heave_geo))=0;
yaw_geo(isnan(yaw_geo))=0;

if size(northing,1)>1
    northing=northing';
    easting=easting';
end

BeamAngleAthwartship=trans_obj.get_params_value('BeamAngleAthwartship',idx_ping,idx_beam);
BeamAngleAlongship=trans_obj.get_params_value('BeamAngleAlongship',idx_ping,idx_beam);

data_to_pos = p.Results.data_to_pos;
detection_mask = p.Results.detection_mask;
for uid = 1:numel(data_to_pos)
    heave_geo_tmp = heave_geo;
    roll_geo_tmp = roll_geo;
    pitch_geo_tmp = pitch_geo;
    yaw_geo_tmp = yaw_geo;
    heading_geo_tmp = heading_geo;
    easting_tmp = easting;
    northing_tmp = northing;
    zone_tmp = zone_tt;
    idx_ping_curr = idx_ping;

    switch data_to_pos{uid}
        case 'WC'
            if comp_angle
                AcrossAngle=trans_obj.Data.get_subdatamat('idx_r',idx_r,'idx_ping',idx_ping_curr,'idx_beam',idx_beam,'field','AcrossAngle');
                AlongAngle=trans_obj.Data.get_subdatamat('idx_r',idx_r,'idx_ping',idx_ping_curr,'idx_beam',idx_beam,'field','AlongAngle');
            else
                AlongAngle=zeros(numel(idx_r),numel(idx_ping_curr),numel(idx_beam));
                AcrossAngle=zeros(numel(idx_r),numel(idx_ping_curr),numel(idx_beam));
            end


            if isempty(AlongAngle)
                AlongAngle=zeros(size(data_disp));
            end

            if isempty(AcrossAngle)
                AcrossAngle=zeros(size(data_disp));
            end

            r_data=repmat(trans_obj.get_transceiver_range(idx_r),1,numel(idx_ping_curr),numel(idx_beam));


        case 'bottom'
            r_data = trans_obj.get_bottom_range(idx_ping_curr);
            AlongAngle=zeros(size(r_data));
            AcrossAngle=zeros(size(r_data));
        case 'transducer'
            r_data = trans_obj.get_transducer_depth(idx_ping_curr);
            AlongAngle=zeros(size(r_data));
            AcrossAngle=zeros(size(r_data));
            BeamAngleAthwartship=zeros(size(r_data));
            BeamAngleAlongship=zeros(size(r_data));
        case {'singletarget','tracktarget'}


            if isempty(trans_obj.ST)
                warndlg_perso([],'','No single targets');
                continue;
            end

            idx_keep_p=find(ismember(trans_obj.ST.Ping_number,idx_ping_curr));
            idx_keep_r=find(ismember(trans_obj.ST.idx_r,idx_r));
            idx_keep=intersect(idx_keep_r,idx_keep_p);

            if strcmpi(data_to_pos{uid},'trackedtarget')
                if isempty(trans_obj.Tracks)
                    warndlg_perso([],'','No tracked targets');
                    continue;
                end
                idx_keep_ori=idx_keep;
                idx_keep=[];
                for i=1:numel(trans_obj.Tracks.target_id)
                    idx_keep=union(idx_keep,intersect(idx_keep_ori,trans_obj.Tracks.target_id{i}));
                end
            end

            if isempty(idx_keep)
                continue;
            end

            r_data=trans_obj.ST.Target_range(idx_keep);
            AlongAngle=trans_obj.ST.Angle_minor_axis(idx_keep);
            AcrossAngle=trans_obj.ST.Angle_major_axis(idx_keep);
            idx_ping_curr=trans_obj.ST.Ping_number(idx_keep);

            BeamAngleAthwartship=zeros(size(idx_ping_curr));
            BeamAngleAlongship=zeros(size(idx_ping_curr));

            heave_geo_tmp = trans_obj.ST.Heave(idx_keep);
            roll_geo_tmp = trans_obj.ST.Roll(idx_keep);
            pitch_geo_tmp = trans_obj.ST.Pitch(idx_keep);
            yaw_geo_tmp = trans_obj.ST.Yaw(idx_keep);
            heading_geo_tmp = trans_obj.ST.Heading(idx_keep);
            easting_tmp = easting(idx_ping_curr-idx_ping(1)+1);
            northing_tmp = northing(idx_ping_curr-idx_ping(1)+1);
            zone_tmp = zone_tmp(idx_ping_curr-idx_ping(1)+1);

    end

    nb_samples = size(r_data,1);
    nb_beams = size(r_data,3);

    Heave = repmat(heave_geo_tmp,nb_samples,1,nb_beams);
    BAR = BeamAngleAthwartship+repmat(roll_geo_tmp,nb_samples,1,nb_beams);
    BAP = BeamAngleAlongship+repmat(pitch_geo_tmp,nb_samples,1,nb_beams);
    Yaw = repmat(yaw_geo_tmp,nb_samples,1,nb_beams);
    zone_tmp = repmat(zone_tmp,nb_samples,1,nb_beams);
    
    if ~isempty(detection_mask) && strcmpi(data_to_pos{uid},'WC')
        r_data = r_data(detection_mask)';
        AcrossAngle = AcrossAngle(detection_mask)';
        AlongAngle = AlongAngle(detection_mask)';
        Heave = Heave(detection_mask)';
        BAR = BAR(detection_mask)';
        BAP = BAP(detection_mask)';
        Yaw=  Yaw(detection_mask)';
        zone_tmp = zone_tmp(detection_mask)';

        heading_geo_tmp = repmat(heading_geo_tmp,nb_samples,1,nb_beams);
        heading_geo_tmp = heading_geo_tmp(detection_mask)';
        idx_ping_curr = repmat(idx_ping_curr,nb_samples,1,nb_beams);
        idx_ping_curr = idx_ping_curr(detection_mask)';

        easting_tmp = repmat(easting_tmp,nb_samples,1,nb_beams);
        easting_tmp = easting_tmp(detection_mask)';
        northing_tmp = repmat(northing_tmp,nb_samples,1,nb_beams);
        northing_tmp = northing_tmp(detection_mask)';

    end

    [Along_dist,Across_dist,Z_t]=arrayfun(@angles_to_pos_single,...
        r_data,...
        AcrossAngle,...
        AlongAngle,...
        Heave,...
        BAR,...
        BAP,...
        Yaw,...
        trans_obj.Config.TransducerAlphaX*ones(size(r_data)),....
        trans_obj.Config.TransducerAlphaY*ones(size(r_data)),....
        trans_obj.Config.TransducerAlphaZ*ones(size(r_data)),....
        trans_obj.Config.TransducerOffsetX*ones(size(r_data)),....
        trans_obj.Config.TransducerOffsetY*ones(size(r_data)),....
        trans_obj.Config.TransducerOffsetZ*ones(size(r_data)));

    trans_depth=trans_obj.get_transceiver_top_depth(idx_ping_curr);

    H.(data_to_pos{uid})=Z_t+trans_depth;

    E.(data_to_pos{uid})=easting_tmp+Across_dist.*cosd(heading_geo_tmp)+Along_dist.*sind(heading_geo_tmp);%W/E
    N.(data_to_pos{uid})=northing_tmp-Across_dist.*sind(heading_geo_tmp)+Along_dist.*cosd(heading_geo_tmp);%N/S
    Along_dist = Along_dist+trans_obj.GPSDataPing.Dist(idx_ping_curr);

    zone.(data_to_pos{uid}) = zone_tmp;


end
