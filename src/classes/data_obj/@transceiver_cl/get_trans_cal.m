
function cal=get_trans_cal(trans_obj)

    G0=trans_obj.get_current_gain();
    SACORRECT=trans_obj.get_current_sacorr();
    EQA=trans_obj.Config.EquivalentBeamAngle;
    alpha=mean(trans_obj.get_absorption(),'all','omitnan')*1e3;

    cal=struct('G0',G0,'SACORRECT',SACORRECT,'EQA',EQA,'alpha',alpha);   
    
    cal = trans_obj.Config.get_cal_fields(cal);
     
end