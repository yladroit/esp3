function [idx_r,idx_ping] = get_idx_r_idx_ping(trans_obj,region,varargin)

%% input parser
p = inputParser;
addRequired(p,'trans_obj',@(x) isa(x,'transceiver_cl'));
addRequired(p,'region',@(x) isa(x,'region_cl'));
addParameter(p,'timeBounds',[0 Inf],@isnumeric);
addParameter(p,'depthBounds',[-inf inf],@isnumeric);
addParameter(p,'rangeBounds',[-inf inf],@isnumeric);
addParameter(p,'refRangeBounds',[-inf inf],@isnumeric);
parse(p,trans_obj,region,varargin{:});

idx_ping_tot = region.Idx_ping;
idx_r = [];
idx_ping = [];

idx_ping_tot(idx_ping_tot>numel(trans_obj.get_transceiver_time())) = [];

time_tot = trans_obj.get_transceiver_time(idx_ping_tot);

idx_keep_x = ( time_tot<=p.Results.timeBounds(2) & time_tot>=p.Results.timeBounds(1) );

if ~any(idx_keep_x)
    return;
end

idx_ping = idx_ping_tot(idx_keep_x);
idx_r = region.Idx_r;


range_trans = trans_obj.get_transceiver_range(idx_r);
idx_r = idx_r(range_trans>nanmin(p.Results.rangeBounds)&range_trans<=nanmax(p.Results.rangeBounds));

depth_trans = trans_obj.get_transceiver_depth(idx_r,idx_ping);
idx_r = idx_r(any(nansum(depth_trans>p.Results.depthBounds(1)&depth_trans<=p.Results.depthBounds(2),2)>0,3));