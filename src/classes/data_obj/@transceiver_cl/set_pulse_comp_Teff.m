function set_pulse_comp_Teff(trans_obj)

[~,y_tx_matched,~]=trans_obj.get_pulse();
y_tx_auto=xcorr(y_tx_matched,'normalized');
tmp=(nansum(abs(y_tx_auto).^2)/(nanmax(abs(y_tx_auto).^2))).*trans_obj.get_params_value('SampleInterval');
if~isempty(tmp)
    trans_obj.Params.TeffCompPulseLength=tmp(:,trans_obj.Params.PingNumber);
end
end