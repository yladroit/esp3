function idx_beam = get_idx_beams(trans_obj,beam_angular_limit)

if trans_obj.ismb
    BWA=trans_obj.get_params_value('BeamAngleAthwartship',1);
    if isempty(beam_angular_limit)
        beam_angular_limit = [nanmin(BWA) nanmax(BWA)];
    end
    if isempty(beam_angular_limit)
        beam_angular_limit = [nanmin(BWA) nanmax(BWA)];
    end
    
    idx_beam = find(BWA>=beam_angular_limit(1) & BWA<=beam_angular_limit(2));
else
    idx_beam = 1;
end