function set_FT(trans_obj,FT)

block_len=get_block_len(10,'cpu');

trans_obj.Data.replace_sub_data_v2(-999,'feature_sv');
trans_obj.Data.replace_sub_data_v2(0,'feature_id');

if ~isempty(FT)&&~isempty(FT.idx_ping)
    idx_r=min(FT.idx_r):max(FT.idx_r);
    idx_ping_tot=min(FT.idx_ping):max(FT.idx_ping);
    idx_beam=min(FT.idx_beam):max(FT.idx_beam);
    
    block_size = min(ceil(block_len/numel(idx_r)),numel(idx_ping_tot));
    num_ite = ceil(numel(idx_ping_tot)/block_size);
    
    for ui=1:num_ite
        idx_ping=idx_ping_tot((ui-1)*block_size+1:min(ui*block_size,numel(idx_ping_tot)));
        
        dataMat=-999*ones(numel(idx_r),numel(idx_ping),numel(idx_beam));
        dataMatid=zeros(numel(idx_r),numel(idx_ping),numel(idx_beam));

        idx_feature=find(ismember(FT.idx_ping,idx_ping));

        idx_r_ft=FT.idx_r(idx_feature)-idx_r(1)+1;
        idx_ping_ft=FT.idx_ping(idx_feature)-idx_ping(1)+1;
        idx_beam_ft=FT.idx_beam(idx_feature)-idx_beam(1)+1;
        
        for it=1:numel(idx_r_ft)
            dataMat(idx_r_ft(it),idx_ping_ft(it),idx_beam_ft(it))=FT.sv(idx_feature(it));
            dataMatid(idx_r_ft(it),idx_ping_ft(it),idx_beam_ft(it))=FT.id(idx_feature(it));
        end
        
        trans_obj.Data.replace_sub_data_v2(dataMat,'feature_sv','idx_r',idx_r,'idx_ping',idx_ping,'idx_beam',idx_beam);
        trans_obj.Data.replace_sub_data_v2(dataMatid,'feature_id','idx_r',idx_r,'idx_ping',idx_ping,'idx_beam',idx_beam);
    end
end

trans_obj.FT=FT;

