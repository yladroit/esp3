function [data_struct,no_nav,zone] = get_region_3D_echoes(reg_obj,trans_obj,varargin)

%% input variable management

p = inputParser;

% default values
field_def='sp';

[cax_d,~,~]=init_cax(field_def);
addRequired(p,'reg_obj',@(obj) isa(obj,'region_cl'));
addRequired(p,'trans_obj',@(obj) isa(obj,'transceiver_cl')|isstruct(obj));
addParameter(p,'Name',reg_obj.print(),@ischar);
addParameter(p,'Cax',cax_d,@isnumeric);
addParameter(p,'Cmap','ek60',@ischar);
addParameter(p,'alphadata',[],@isnumeric);
addParameter(p,'field',field_def,@ischar);
addParameter(p,'other_fields',{'bottom','transducer'},@iscell);
addParameter(p,'trackedOnly',false,@islogical);
addParameter(p,'comp_angle',1,@isnumeric);
addParameter(p,'roll_comp',~trans_obj.ismb,@islogical);
addParameter(p,'pitch_comp',~trans_obj.ismb,@islogical);
addParameter(p,'yaw_comp',~trans_obj.ismb,@islogical);
addParameter(p,'thr',nan,@isnumeric);
addParameter(p,'main_figure',[],@(h) isempty(h)|ishghandle(h));
addParameter(p,'parent',[],@(h) isempty(h)|ishghandle(h));
addParameter(p,'load_bar_comp',[]);

parse(p,reg_obj,trans_obj,varargin{:});

field=p.Results.field;



comp_angle=p.Results.comp_angle;


data_struct=[];
no_nav=0;
zone='';

if isempty(reg_obj)
    idx_ping=trans_obj.get_transceiver_pings();
    idx_r=trans_obj.get_transceiver_samples();
else
    idx_ping=reg_obj.Idx_ping;
    idx_r=reg_obj.Idx_r;
end


switch field
    case 'singletarget'

        if isempty(trans_obj.ST)
            return;
        end

        idx_keep_p=find(ismember(trans_obj.ST.Ping_number,idx_ping));
        idx_keep_r=find(ismember(trans_obj.ST.idx_r,idx_r));
        idx_keep=intersect(idx_keep_r,idx_keep_p);

        if p.Results.trackedOnly
            field_to_pos = 'trackedtarget';
            if isempty(trans_obj.Tracks)
                warndlg_perso([],'','No tracked targets');
                return;
            end
            idx_keep_ori=idx_keep;
            idx_keep=[];
            for i=1:numel(trans_obj.Tracks.target_id)
                idx_keep=union(idx_keep,intersect(idx_keep_ori,trans_obj.Tracks.target_id{i}));
            end
            
        else
            field_to_pos = 'singletarget';
        end

        if isempty(idx_keep)
            warndlg_perso([],'','No single targets');
            return;
        end

       
        data_disp=trans_obj.ST.TS_comp(idx_keep);
        compensation=zeros(size(data_disp));
        Mask=ones(size(data_disp));
        idx_beam = 1;

    otherwise
        switch field
            case 'TS'
                field_load='sp';
            case {'sv' 'svdenoised'}
                field_load=field;
            otherwise
                field_load=field;
        end


        [data_disp,idx_r,idx_ping,bad_data_mask,bad_trans_vec,~,below_bot_mask,~]=trans_obj.get_data_from_region(reg_obj,...
            'field',field_load);
        if isempty(data_disp)
            return;
        end
        dr = 1;
        db = 1;

        nb_beams = max(trans_obj.Data.Nb_beams,[],'omitnan');

        idx_beam = 1:db:nb_beams;

        idx_r = idx_r(1:dr:end);

        data_disp = data_disp(idx_r-idx_r(1)+1,:,idx_beam);

        Mask=(~bad_data_mask)&(~below_bot_mask(:,:,idx_beam));
        Mask(:,bad_trans_vec,:)=0;
        Mask = Mask(idx_r-idx_r(1)+1,:,:);

        AcrossAngle=trans_obj.Data.get_subdatamat('idx_r',idx_r,'idx_ping',idx_ping,'idx_beam',idx_beam,'field','AcrossAngle');
        AlongAngle=trans_obj.Data.get_subdatamat('idx_r',idx_r,'idx_ping',idx_ping,'idx_beam',idx_beam,'field','AlongAngle');

        if comp_angle==0 || isempty(AlongAngle)
            AlongAngle=zeros(size(data_disp));  
        else

        end
        if comp_angle==0 || isempty(AcrossAngle)
            AcrossAngle=zeros(size(data_disp));
        end

        [faBW,psBW] = trans_obj.get_beamwidth_at_f_c([]);
        compensation = simradBeamCompensation(faBW,...
            psBW, AlongAngle, AcrossAngle);

        field_to_pos = 'WC';

end


if isempty(data_disp)
    disp_perso([],sprintf('Field %s not found. Cannot get 3D region',field));
    return;
end

[fields_tot,scale_fields,fmt_fields,factor_fields,default_values]=init_fields();

idx_field=strcmpi(fields_tot,field);
detection_mask = [];

if ~isempty(idx_field)
    detection_mask = data_disp>default_values(idx_field);
end

all_fields = union(p.Results.other_fields,field_to_pos);

[E,N,H,zone,no_nav]=trans_obj.get_xxx_ENH('data_to_pos',all_fields,...
    'idx_r',idx_r,'idx_ping',idx_ping,'idx_beam',idx_beam,...
    'detection_mask',detection_mask,...
    'comp_angle',ismember(field,{'sp','spdenoised'}),...
    'yaw_comp',p.Results.yaw_comp,...
    'roll_comp',p.Results.roll_comp,...
    'pitch_comp',p.Results.pitch_comp);


if ~no_nav && isfield(N,field_to_pos)
    [Lat_t,Lon_t] = utm2ll(E.(field_to_pos)(:),N.(field_to_pos)(:),zone.(field_to_pos)(:));
else
    Lat_t=N_t;
    Lon_t=E_t;
end

t=trans_obj.get_transceiver_time(idx_ping);

data_struct.data_disp=data_disp(detection_mask);
data_struct.mask=Mask(detection_mask);
data_struct.compensation=compensation(detection_mask);

if isfield(E,field_to_pos)
    data_struct.N_t=N.(field_to_pos)(:);
    data_struct.E_t=E.(field_to_pos)(:);
    data_struct.depth=H.(field_to_pos)(:);
end

if isfield(E,'bottom')
    data_struct.E_bottom=E.bottom(:);
    data_struct.N_bottom=N.bottom(:);
    data_struct.depth_bottom=H.bottom(:);
end

if isfield(E,'transducer')
    data_struct.E_transd=E.transducer(:);
    data_struct.N_transd=N.transducer(:);
    data_struct.depth_transd=H.transducer(:);
end

data_struct.ping_num_vessel=idx_ping(:);

data_struct.time=t(:);

data_struct.lat=Lat_t(:);
data_struct.lon=Lon_t(:);

data_struct.zone = zone.(field_to_pos)(:);




end