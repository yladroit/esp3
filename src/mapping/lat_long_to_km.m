function [dist_in_km,heading_in_deg] = lat_long_to_km(lat,lon)

%nb_pt = numel(lat);

if isrow(lat)
    lat=lat';
end

if isrow(lon)
    lon=lon';
end


try
    nb_pt = numel(lat);
    %     [easting,northing,~]=deg2utm(lat,lon);
    %     id = find(abs(diff(double(lon>180)-double(lon<=180)))>0);
    %
    %     for uid = 1:numel(id)
    %         easting(id+1:end) = easting(id+1:end)-easting(id+1)+easting(id);
    %     end
    %
    %     dist_in_km = sqrt(diff(easting).^2+diff(northing).^2)/1e3;
    %     dist_in_km = dist_in_km(:)';
    %     heading_in_deg = atand(diff(easting)./diff(northing));
    
    [dist_in_deg,heading_in_deg] = distance([lat(1:nb_pt-1) lon(1:nb_pt-1)],[lat(2:nb_pt) lon(2:nb_pt)]);
    dist_in_km  = deg2km(dist_in_deg);
    
    dist_in_km = dist_in_km(:)';
    heading_in_deg = heading_in_deg(:)';
    
catch err
    print_errors_and_warnings([],'warning',err);
    dist_in_km=zeros(1,numel(lat)-1);
    heading_in_deg=zeros(1,numel(lat)-1);
end
