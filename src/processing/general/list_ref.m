function ref_cell  = list_ref(varargin)
    ref_cell = {'Transducer' 'Surface' 'Bottom'};
    if nargin>=1
        ref_cell = ref_cell{varargin{1}};
    end
end