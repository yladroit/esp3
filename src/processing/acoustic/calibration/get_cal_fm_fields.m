
function fm_fields = get_cal_fm_fields()

fm_fields = {'Frequency' 'Gain' 'BeamWidthAlongship' 'BeamWidthAthwartship' 'AngleOffsetAlongship' 'AngleOffsetAthwartship'};